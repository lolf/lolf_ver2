using UnityEngine;
using UnityEditor;
using System.Collections;

public class EDITOR_SetPhysicsMaterialWizard : ScriptableWizard {
    public PhysicMaterial material;

    [MenuItem ("Custom/SetPhysicsMaterial...")]
    static void CreateWizard() {
        ScriptableWizard.DisplayWizard<EDITOR_SetPhysicsMaterialWizard>("Set Physics Material Recursively", "Set!");
    }
    
    //Main function
    public void OnWizardCreate() {
        int total = 0;
        foreach( Transform e in Selection.transforms) { 
          total += recurse(e, material);
        }
        if (total == 0)
            Debug.Log("No Physics Materials Changed.");
        else
            Debug.Log(total + " Physics Materials changed");
    }
    
    public int recurse(Transform parent, PhysicMaterial material){
        int total = 0;
        foreach (Transform child in parent) {
            total += recurse(child,material);
        }
        //add component to parent
        Collider c = parent.GetComponent<Collider>();
        if (c) {
            c.collider.material = material;
            total++;
        }
        
        return total;
    }
    //Set the help string
    public void OnWizardUpdate () {  
        helpString = "Specify the physics mateiral you wish to set";
    }
    
    // The menu item will be disabled if no transform is selected. 
    [MenuItem ("GameObject/Add Component Recursively...", true)]
    static bool ValidateMenuItem(){ 
       return Selection.activeTransform; 
    }
}
