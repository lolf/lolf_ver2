using UnityEngine;
using System.Collections;

public class FlatCameraManager{
    
    public Camera Camera{ get; private set; }

	//TODO do not use this stupid thing...
    public CameraInterpolator Interpolator
    { get; private set; }

    public Vector3 Center
    {
        get;
        private set;
    }
    public float Distance 
    { 
        get; 
        private set; 
    }
    public bool IsOrthographic
    {
        get { return Camera.isOrthoGraphic; }
        private set { Camera.isOrthoGraphic = true; }
    }
    public float Width
    {
        get
        {
            if (IsOrthographic)
                return Camera.orthographicSize * Camera.aspect * 2;
            else
                return Distance * Mathf.Tan(Camera.fov / 2.0f);
        }
    }
	
    public float Height
    {
        get { return Width / Camera.aspect; }
    }
	
	public Vector2 Size
	{
		get { return new Vector2(Width,Height);}
	}

    
    public FlatCameraManager(Vector3 aCenter, float aDistance)
    {
        Center = aCenter;
        Distance = aDistance;
        create_camera();
        IsOrthographic = true;
        Interpolator = new CameraInterpolator(this.Camera);
    }
	
    public void update(float aDeltaTime)
    {
		//chis is dumb. we don't need the camera interplotaor...
        Interpolator.update(aDeltaTime);
    }
	
	//TODO make sure so camera is not backwards...
    //for setting camera
    void create_camera()
    {
        Camera = (new GameObject("genFlatCamera")).AddComponent<Camera>();
        Camera.transform.position = Center + Vector3.forward * Distance;
        Camera.transform.LookAt(Center, Vector3.up);
        Camera.nearClipPlane = 0.1f;
        Camera.farClipPlane = 1000;
        Camera.depth = 100;
        Camera.clearFlags = CameraClearFlags.Depth;
    }
	
	//TODO rename these functions, make them call each other pfftt
	
	public static void fit_camera_to_screen(Camera aCam)
	{
		//comment out this function to disable black bars
		float screenRatio = Screen.width / (float)Screen.height;
		float desiredAspect = screenRatio;
		Rect newRect = aCam.rect;
        if (desiredAspect > screenRatio) //match camera width to screen width
		{
			float yGive = screenRatio/desiredAspect; //desiredHeight to screenHeight
			newRect.y = (1-yGive)/2;
			newRect.height = yGive;
		}
        else
		{
			float xGive = desiredAspect/screenRatio; //screen width to camera width
			newRect.x = (1-xGive)/2;
			newRect.width = xGive;
		}
		aCam.rect = newRect;
		aCam.aspect = desiredAspect;
	}
	
	public void fit_camera_to_screen(bool hard = true)
	{
		float desiredHeight = Screen.height;
		Interpolator.TargetOrthographicHeight = desiredHeight/2f;
		Camera.orthographicSize = desiredHeight/2f; //don't thin kI need this but just in case...
		//this is for black bars
		if(hard)
			fit_camera_to_screen(Camera);
		Camera.orthographicSize = desiredHeight/2f;
	}
	
    public void focus_camera_on_element(FlatElementBase aElement)
    {
        Rect focus = aElement.BoundingBox;
             //TODO what if camera is not orthographic
        float texRatio = focus.width / (float)focus.height;
        float camRatio = this.Camera.aspect;
        if (camRatio > texRatio) //match width
            Interpolator.TargetOrthographicHeight = (focus.width / camRatio) / 2.0f;
        else
            Interpolator.TargetOrthographicHeight = (focus.height) / 2.0f;
        Vector3 position = aElement.HardPosition;
        position.z = Center.z + Distance;
        Interpolator.TargetSpatialPosition = new SpatialPosition(position, Camera.transform.rotation);
    }
	
	public Vector3 screen_pixels_to_camera_pixels(Vector3 aVal)
	{
		return new Vector3(aVal.x * Width/(Camera.rect.width*Screen.width),aVal.y * Height/(Camera.rect.height*Screen.height),aVal.z);
	}

    //other stuff
    //returns world point from coordinates relative to center of screen where screen is (-1,1)x(-1,1)
    public Vector3 get_point(float aX, float aY)
    {
        return Center + new Vector3(aX * Width / 2.0f, aY * Height / 2.0f, 0);
    }
	
	//this version measures from the entire screen. Not the visible portion
	//TODO does not account for not centered camera, i.e. modify by Camera.rect.x/y
	public Vector3 get_point_total(float aX, float aY)
	{
		return Center + new Vector3(aX * Width * (1/Camera.rect.width) / 2.0f, aY * Height * (1/Camera.rect.height) / 2.0f, 0);
	}
}
