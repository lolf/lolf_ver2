using UnityEngine;
using System.Collections;

public static class KeyboardUtilities 
{
	public static Vector3 get_keyboard_as_vector()
	{
		Vector3 r = Vector3.zero;
		if(Input.GetKey(KeyCode.A))
			r.x -= 1;
		if(Input.GetKey(KeyCode.D))
			r.x += 1;
		if(Input.GetKey(KeyCode.W))
			r.y += 1;
		if(Input.GetKey(KeyCode.S))
			r.y -= 1;
		return r;
	}
}
