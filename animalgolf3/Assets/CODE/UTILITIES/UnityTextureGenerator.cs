using UnityEngine;
using System.Collections.Generic;

public static class UnityTextureGenerator {
    static Dictionary<Color, Texture2D> mColorTextures = new Dictionary<Color, Texture2D>();
    public static Texture2D get_color_texture(Color aColor)
    {
        if (!mColorTextures.ContainsKey(aColor))
        {
            int w = 2;
            int h = 2;
            Texture2D rgb_texture = new Texture2D(w, h);
            Color rgb_color = aColor;
            int i, j;
            for (i = 0; i < w; i++)
            {
                for (j = 0; j < h; j++)
                {
                    rgb_texture.SetPixel(i, j, rgb_color);
                }
            }
            rgb_texture.Apply();
            mColorTextures[aColor] = rgb_texture;
        }
        return mColorTextures[aColor];
    }
}
