using UnityEngine;
using System.Collections;

public static class GameObjectUtilities {

	public static Bounds union_mesh_bounds(this GameObject o)
	{
		Bounds b = new Bounds();
		bool first = true;
		foreach(Renderer e in o.GetComponentsInChildren<Renderer>())
		{
			if(first)
			{
				b = e.bounds;
				first = false;
			}
			else
			{
				b.Encapsulate(e.bounds.min);
				b.Encapsulate(e.bounds.max);
			}
		}
		return b;
	}
	
	public static Vector3 mesh_center(this GameObject o)
	{
		return o.union_mesh_bounds().center;
	}
}
