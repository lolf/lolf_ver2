using UnityEngine;
using System.Collections;

public class SpaceGravity : MonoBehaviour {
	
	public GameObject[] orbitalBodies;
	
	void Start () {
		Physics.gravity = Vector3.zero;
		
		CentralManager.Manager.get_up_vector = getUpVector;
		CentralManager.Manager.get_gravity_vector = getGravity;
		
		//TODO this is probbaly wrong.
		CentralManager.Manager.is_in_bounds = 
			delegate(Vector3 arg) {
			{
				return true;
				//return get_gravity(arg).magnitude < 0.01f;
			}
		};
	}
	
	// Update is called once per frame
	public Vector3 getUpVector (Vector3 aPos) 
    {
        Vector3 up = Vector3.zero;
        foreach (GameObject e in orbitalBodies)
        {
            Vector3 dir = e.transform.position - aPos;
            Vector3 force = (dir.normalized * 10000000 * e.rigidbody.mass);
            force = force / (dir.magnitude * dir.magnitude);
            up -= force;
        }
        return up.normalized;
	}

    public Vector3 getGravity(Vector3 aPos)
    {
        Vector3 up = Vector3.zero;
        foreach (GameObject e in orbitalBodies)
        {
            Vector3 dir = e.transform.position - aPos;
            Vector3 force = (dir.normalized * 1000 * e.rigidbody.mass);
            force = force / (dir.magnitude * dir.magnitude);
            up += force;
        }
        return up; //should mulitply by ball mass for force on  ball duh
    }
}
