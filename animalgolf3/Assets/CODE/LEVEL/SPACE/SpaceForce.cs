using UnityEngine;
using System.Collections.Generic;

public class SpaceForce : MonoBehaviour {
	//TODO
    public GameObject[] mBodies;
	void FixedUpdate () 
    {
        foreach (GameObject e in mBodies)
        {
            Vector3 dir = e.transform.position - transform.position;
            Vector3 force = (dir.normalized * 1000 * e.rigidbody.mass * transform.rigidbody.mass);
            force = force / (dir.magnitude * dir.magnitude);
            rigidbody.AddForce(force);
        }
	}
	
}
