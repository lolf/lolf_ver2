using UnityEngine;
using System.Collections.Generic;

public class SpaceManager
{
	LinkedList<GameObject> SpaceObjects
	{get; set;}
	
	public SpaceManager()
	{
		SpaceObjects = new LinkedList<GameObject>();
	}
	
	public void find_space_objects()
	{
		object[] allObjects = GameObject.FindObjectsOfTypeAll(typeof(GameObject)) ;
		foreach(object thisObject in allObjects)
		   if (((GameObject) thisObject).activeInHierarchy)
		      Debug.Log(thisObject+" is an active object") ;
	}
}
