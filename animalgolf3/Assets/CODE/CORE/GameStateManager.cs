using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	public enum GameState
	{
		GS_NONE,
		GS_PREVIEW,
		GS_USER,
		GS_SHOOTING,
		GS_OOB,
		GS_WIN
	}
	

	GameState mState;
	GameState mPrevState;
	float mStateTime;
	float mBeginTime = 0;
	
	public GameState get_state(){return mState;}
	public void set_state(GameState aState)
	{
		if(mPrevState == GameState.GS_PREVIEW)
			mBeginTime = Time.time;
		mState = aState;
		if(mPrevState != mState)
			Manager.trigger_event("game_state_changed");
		mPrevState = mState;
		mStateTime = Time.time;
		Debug.Log (mState);
	}
	public float time_on_state(){return Time.time-mStateTime;}
	public float time_since_begin(){if(get_state() == GameState.GS_PREVIEW) return 0; else return Time.time-mBeginTime;}
	public void Start () {
		mPrevState = GameState.GS_NONE;
		set_state(GameState.GS_PREVIEW);
		
	}
	public void Update () {
	}
	
	
	public void evt_ball_stabalized()
	{
		if(mState == GameState.GS_SHOOTING)
			set_state(GameState.GS_USER);
	}
	
	public void evt_ball_hit()
	{
		set_state(GameState.GS_SHOOTING);
	}
	
	public void evt_ball_oob()
	{
		set_state(GameState.GS_OOB);
	}
}
