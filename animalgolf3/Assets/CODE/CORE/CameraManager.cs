using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CameraManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	
	public enum CameraEnum
	{
		BALL,INTRO,SHOT,WIN,OVERHEAD
	}
	
	public GameObject Cam {get; private set;} //the real camera
	Dictionary<CameraEnum,GameObject> mCamMap = new Dictionary<CameraEnum, GameObject>();
	GameObject mBall;
	
	QuTimer mTimer = new QuTimer(0,1);
	CameraEnum mPrevCam = CameraEnum.INTRO; // don't really need this...
	CameraEnum mTarCam = CameraEnum.INTRO;
	
	
	public void Start () {
		
		mBall = Manager.Ball;
		
		mCamMap[CameraEnum.INTRO] = Manager.mIntroCameraManager.mIntroCamera;
		mCamMap[CameraEnum.SHOT] = Manager.mBallCameraManager.mBallCamera;
		mCamMap[CameraEnum.BALL] = Manager.mScenicCameraManager.mScenicCamera;
		mCamMap[CameraEnum.OVERHEAD] = Manager.mElementManager.mOverheadCameraPosition;
		mCamMap[CameraEnum.WIN] = Manager.mBallCameraManager.mBallCamera;
		
		
		Cam = new GameObject("genMainCamera");
		Cam.AddComponent<Camera>();
		Cam.AddComponent<GUILayer>();
		//Cam.camera.backgroundColor = new Color(0,0,0,1);
		//Cam.camera.clearFlags = CameraClearFlags.SolidColor;
		Cam.camera.clearFlags = CameraClearFlags.Depth;
		
		set_camera(CameraEnum.INTRO);
	}
	public void Update () {
		//TODO smooth transitioning??
		//read game state and control which camera we are on...
		mTimer.update(Time.deltaTime);
		Cam.transform.position = mCamMap[mTarCam].transform.position;
		Cam.transform.rotation = mCamMap[mTarCam].transform.rotation;
		//mCamera.transform.LookAt(mBall.transform);
		
		//cute little hack
		if(Input.GetKey(KeyCode.Alpha1))
		{
			Cam.transform.position = mCamMap[CameraEnum.OVERHEAD].transform.position;
			Cam.transform.rotation = mCamMap[CameraEnum.OVERHEAD].transform.rotation;
		}
			
	}
	
	public void set_camera(CameraEnum cam)
	{
		mPrevCam = mTarCam;
		mTarCam = cam;
		mTimer.reset();
	}
	
	public Camera get_camera() { return Cam.camera; }
	
		
	public void evt_game_state_changed()
	{
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_USER)
		{
			set_camera(CameraEnum.SHOT);
		}
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_SHOOTING)
			set_camera(CameraEnum.BALL);
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_WIN)
			set_camera(CameraEnum.WIN);
			
	}
}
