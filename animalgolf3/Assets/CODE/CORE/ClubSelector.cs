using UnityEngine;
using System.Collections;

public class ClubSelector {
	
	CentralManager mManager;
	GameObject mCamera;
	GameObject mParentClub;
	GameObject[] mClubs;
	QuTimer mTimer = new QuTimer(0,0.5f);
	int mIndex = 0;
	float mCurrentIndex = 0;
	public ClubSelector(CentralManager aManager)
	{
		mManager = aManager;
		mParentClub = new GameObject("genParentClub");
		mCamera = new GameObject("genClubSelectorCamera");
		mCamera.AddComponent<Camera>();
		mCamera.camera.clearFlags = CameraClearFlags.Depth;
		Rect cr = mCamera.camera.pixelRect;
		cr.height = Mathf.Min(0.5f*Screen.height,300);
		cr.width = cr.height * 0.2f;
		cr.x = 0.02f*Screen.width;
		cr.y = 0.5f*cr.height;
		mCamera.camera.pixelRect = cr;
		
		
		mParentClub.transform.position = new Vector3(1000,0,0);
		mCamera.transform.position = mParentClub.transform.position + new Vector3(0,-2,0);
		mCamera.transform.LookAt(mParentClub.transform);
		mClubs = new GameObject[mManager.mRef.mPrefabRef.mClubs.Length];
		for(int i = 0; i < mManager.mRef.mPrefabRef.mClubs.Length; i++)
		{
			mClubs[i] = (GameObject)GameObject.Instantiate(mManager.mRef.mPrefabRef.mClubs[i],Vector3.zero,Quaternion.identity); 
			mClubs[i].AddComponent<ConstantRotation>().set_rotation(new Quaternion(0.5f,0.5f,0.5f,0.5f));
		}
	}
	
	public int get_index(){return mIndex;}
	public void set_target(int index)
	{
		mIndex = Mathf.Clamp(index,0,mClubs.Length);
		mTimer.reset();
	}
	public void Update()
	{
		//figure out our new index
		float prev = mTimer.getOverShot();
		mTimer.update(Time.deltaTime);
		float current = mTimer.getOverShot();
		if(prev == 1)
			mCurrentIndex = mIndex;
		else mCurrentIndex += (mIndex-mCurrentIndex)*(current-prev)/(1-prev);
		
		float totalClubs = mClubs.Length;
		float width = 1; //should determine from screen size??
		float distribution = 0.5f; //distribution in input axis and not in curve length
		//set the transforms of all the objects
		for(int i = 0; i < totalClubs; i++)
		{
			float x = width * (i-mCurrentIndex) * distribution;
			float y = x*x;
			mClubs[i].transform.position = mParentClub.transform.position + new Vector3(0,y,x);
		}
	}
	
	public bool handle_mouse_press(InputManager.MouseProfile mouse)
	{
		Vector3 pos = mouse.get_last_mouse_position_absolute();
		Rect cr = mCamera.camera.pixelRect;
		if(cr.Contains(pos))
		{
			//lamo way first
			if(pos.y > cr.center.y)
				set_target (get_index()+1);
			else set_target(get_index()-1);
			return true;
		}
		return false;
	}
	
	public void handle_mouse_down_motion(InputManager.MouseProfile mouse)
	{
		Vector3 pos = mouse.get_last_mouse_position_absolute();
	}
	
	public void handle_release(InputManager.MouseProfile mouse)
	{
		Vector3 pos = mouse.get_last_mouse_position_absolute();
	}
	
	
}
