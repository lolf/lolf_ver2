using UnityEngine;
using System.Collections;


//this handles hitting 
public class TargetManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}

	GameObject mTarget;		
	public InputManager.MouseEventHandler mMouseHandler = new InputManager.MouseEventHandler();
	
	QuTimer mHitTimer = null;
	public int mShotCount = 0;
	
	public void Start () {
		//hookup mouse events
		mMouseHandler.mMousePressed += this.handle_press;
		mMouseHandler.mMouseMoved += this.handle_mouse_down_motion;
		mMouseHandler.mMouseReleased += this.handle_release;
		
		//Manager.Dump.dump += OnGUI;
		
	}
	
	public void OnGUI()
	{
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_USER)
		{
			GUIStyle skin = new GUIStyle();
			skin.normal.background = UnityTextureGenerator.get_color_texture(new Color(1,1,1,0.7f));
			
			int width = Screen.width/2;
			float padding = 5;
			GUI.Box(new Rect(Screen.width/2-width/2-padding,Screen.height - 100 -padding,width+padding*2,50+padding*2),"",skin);
			if(mHitTimer != null){
				float percent = mHitTimer.getLinear();
				skin.normal.background = UnityTextureGenerator.get_color_texture(new Color(1,0.5f,0.5f,1));
				skin.alignment = TextAnchor.MiddleLeft;
				GUI.Box(new Rect(Screen.width/2-width/2,Screen.height - 100,width*percent,50),percent.ToString(),skin);
			}
		}
	}
	
	
	public void Update () 
	{
		if(mHitTimer != null)
		{
			mHitTimer.update(Time.deltaTime);
		}
		
		if(mHitTimer == null && Input.GetKeyDown(KeyCode.Z))
			mHitTimer = new QuTimer(0,1.7f);
		if(mHitTimer != null && Input.GetKeyUp(KeyCode.Z))
			timer_hit_ball();
	}
	
	//returns true if clicked on animal???
	public bool handle_press(InputManager.MouseProfile mouse)
	{
		RaycastHit hit;
		Ray toCast = Manager.mCameraManager.get_camera().ViewportPointToRay(mouse.get_last_mouse_position_relative());
        if( Physics.Raycast(toCast, out hit, Mathf.Infinity, 1 << Constants.BALL_LAYER))
		{
			mHitTimer = new QuTimer(0,1.7f);
			return true;
		}
		return false;
	}
	
	public void handle_mouse_down_motion(InputManager.MouseProfile mouse){}
	
	public void handle_release(InputManager.MouseProfile mouse)
	{
		timer_hit_ball();
		
	}
	
	public float get_hit_impulse()
	{
		//TODO scale by ball mass as well
		//TODO don't do the stupid default value thing...
		if(mHitTimer == null)
			return Manager.mRef.mSettings.LEVEL_SCALE * 0.75f * 0.5f * Manager.mElementManager.BallMass;
		else
			return mHitTimer.getLinear()*Manager.mRef.mSettings.LEVEL_SCALE*0.75f*Manager.mElementManager.BallMass;
	}
	void timer_hit_ball()
	{
		
		hit_ball(get_hit_impulse());
		mHitTimer = null;
	}
	public void hit_ball(float force)
    { 
		mHitTimer = null;
       	//Manager.Ball.rigidbody.AddForceAtPosition(Manager.mBallCameraManager.get_hit_direction() * force, get_target_position(), ForceMode.Impulse);
		Manager.Ball.rigidbody.AddForce (Manager.mBallCameraManager.get_hit_direction()*force,ForceMode.Impulse);
   		//TODO hit ball
		//TODO trigger hit ball event
		Manager.trigger_event("ball_hit");
		mShotCount++;
    }
	

	
	
	
	
	
	
	
	
	//OLD STUFF
	//UNUSED
	
	
	public void evt_game_state_changed()
	{
		//TODO Delete all this, we don't need it
		//this means either we went from intro to shooting or preview to USER
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_USER)
		{
			//set_target_position(Manager.Ball.rigidbody.worldCenterOfMass,new Vector3(1,0,0));
			//TODO hide the target
			//TODO set a good default shot
		}
		//we just shot the ball
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_SHOOTING)
		{
			//TODO hide the target
		}
	}
	
	InputManager.MouseEventHandler mInternalMouseHandler = new InputManager.MouseEventHandler();
	GameObject mInternalTarget;
	Vector3 mInternalBaseScale = new Vector3(0.3f,0.3f,0.3f);
		//mTarget = (GameObject)GameObject.Instantiate(Manager.mRef.mPrefabRef.mTarget);
		/* put in start function
		mInternalMouseHandler.mMousePressed += this.handle_press_internal;
		mInternalMouseHandler.mMouseMoved += this.handle_mouse_down_motion_internal;
		mInternalMouseHandler.mMouseReleased += this.handle_release_internal;
		mInternalTarget = (GameObject)GameObject.Instantiate(Manager.mRef.mPrefabRef.mTarget);
		mInternalTarget.transform.localScale = mInternalBaseScale;
		mInternalTarget.layer = Constants.INTERNAL_TARGET_LAYER;*/
	
	public Vector3 get_target_position()
	{
		return Manager.Ball.transform.	position;
		//return mTarget.transform.position;
	}
	//------
	//internal target routines
	//------
	public bool handle_press_internal(InputManager.MouseProfile mouse)
	{
		return raycast_to_internal_target(mouse);
	}
	
	public void handle_mouse_down_motion_internal(InputManager.MouseProfile mouse)
	{
		float timeDown = mInternalMouseHandler.time_down();
		float lambda = Mathf.Clamp01(timeDown/2.0f);
		mInternalTarget.transform.localScale = mInternalBaseScale*(1-lambda) + mTarget.transform.localScale*(lambda);
		//TODO set the target
	}
	
	public void handle_release_internal(InputManager.MouseProfile mouse)
	{
		mInternalTarget.transform.localScale = mInternalBaseScale;
		if(raycast_to_internal_target(mouse))
			timer_hit_ball();
	}
	bool raycast_to_internal_target(InputManager.MouseProfile mouse)
	{
		Vector3 pos = InputManager.MouseProfile.to_relative(Manager.mCameraManager.get_camera().WorldToScreenPoint(mInternalTarget.transform.position));
		pos.z = 0;
		if((mouse.get_last_mouse_position_relative()-pos).magnitude < 0.05f)
			return true;
		return false;
		
		//broken???
		RaycastHit hit;
		Ray toCast = Manager.mCameraManager.get_camera().ViewportPointToRay(mouse.get_last_mouse_position_relative());
        bool r = Physics.Raycast(toCast, out hit, Mathf.Infinity, 1 << Constants.INTERNAL_TARGET_LAYER);
		if(hit.collider == mInternalTarget.collider)
			return true;
		return false;
	}
	
	bool raycast_set_target_position(InputManager.MouseProfile mouse)
	{
		RaycastHit hit;
		Ray toCast = Manager.mCameraManager.get_camera().ViewportPointToRay(mouse.get_last_mouse_position_relative());
        if(Physics.Raycast(toCast, out hit, Mathf.Infinity, 1 << Constants.BALL_LAYER))
		{
			set_target_position(hit.point,hit.normal);
			Manager.trigger_event("target_set");
			return true;
		}
		return false;
	}
	
	
	void set_target_position(Vector3 aPos, Vector3 aNorm)
    {
		mTarget.transform.position = aPos;
		mInternalTarget.transform.position = aPos;
		/*
        isSet = true;
        mArrow.transform.position = aPos + aNorm * 0.1f;
        mArrowStart.transform.position = mArrow.transform.position;
        mArrowTarget.transform.position = aPos;
        mArrow.transform.LookAt(mArrowTarget.transform);
        mArrowStart.transform.LookAt(mArrowTarget.transform);*/
    }
}
