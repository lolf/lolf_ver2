using UnityEngine;
using System.Collections;

public class AnimalFact {
	public GameObject mCamera;
	public GameObject mFacts;
	CentralManager mManager;
	public AnimalFact(CentralManager aManager)
	{
		mManager = aManager;
		create_facts();
	}
	
	void create_facts()
	{
		mFacts = new GameObject("genFacts");
		mFacts.AddComponent<AnimalFactAnimation>();
		mFacts.AddComponent<MeshRenderer>();
		TextMesh tm = mFacts.AddComponent<TextMesh>();
		tm.text = "TEMPORARY ANIMAL FACT";
		tm.anchor = TextAnchor.MiddleCenter;
		tm.fontSize = 100;
		mFacts.transform.position = new Vector3(10000,0,0);
		mCamera = new GameObject("genFactCamera");
		mCamera.AddComponent<Camera>();
		mCamera.camera.clearFlags = CameraClearFlags.Depth;
		mCamera.camera.depth = 100;
		mCamera.transform.position = mFacts.transform.position + new Vector3(0,0,50);
		mCamera.transform.LookAt(mFacts.transform);
	}
	
	public void destroy_facts()
	{
		GameObject.DestroyImmediate(mCamera);
		GameObject.DestroyImmediate(mFacts);
		mCamera = null;
		mFacts = null;

	}
}
