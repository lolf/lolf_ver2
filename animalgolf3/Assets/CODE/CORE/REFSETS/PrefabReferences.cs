using UnityEngine;
using System.Collections;

public class PrefabReferences : MonoBehaviour {
	public GameObject mTarget;
	public GameObject[] mClubs;
	public GameObject mPredictorArrows;
}
