using UnityEngine;
using System.Collections;

public class LevelSettings : MonoBehaviour {
	public float LEVEL_SCALE = -1; 	//this should be roughly proportional to distance from bmall to goal
									//this determines how hard ball is hit
	
	//CentralManager must call this function, this will initialize all settings
	public void initialize_default_settings(CentralManager aManager)
	{
		if(LEVEL_SCALE == -1)
		{
			LEVEL_SCALE = aManager.get_distance_from_goal();
				//aManager.mElementManager.StageParent.union_mesh_bounds().extents.magnitude;
		}
	}
	
	

}
