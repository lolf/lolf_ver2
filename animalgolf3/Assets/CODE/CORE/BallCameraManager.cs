using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BallCameraManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	
	public InputManager.MouseEventHandler mMouseHandler = new InputManager.MouseEventHandler();
	public GameObject mBall;
	public GameObject mBallCamera;
	System.Collections.Generic.Queue<Vector3> mPreviousHitDirection = new System.Collections.Generic.Queue<Vector3>();
	
	public float mHitAngle = Mathf.PI/6.0f;
	float mCamDistance = 3;
	
	float mBallScale = 1;
	
	public void Start () {
		mMouseHandler.mMousePressed += this.handle_press;
		mMouseHandler.mMouseMoved += this.handle_mouse_down_motion;
		mMouseHandler.mMouseReleased += this.handle_release;
		mMouseHandler.mPinch += this.handle_pinch;
		
		mBall = Manager.Ball;
		mBallCamera = new GameObject("genShotCamera");
		mBallCamera.AddComponent<SphereCollider>().radius = 0.1f;
		mBallCamera.AddComponent<Rigidbody>();
		mBallCamera.rigidbody.drag = 3;
        mBallCamera.rigidbody.mass = 0.001f;
		mBallCamera.rigidbody.useGravity = false;
		//mBallCamera.transform.position = mBall.transform.position + Manager.get_up_vector(mBall.transform.position)*1;
		mBallCamera.transform.position = Manager.mElementManager.mStartingCameraPosition.transform.position;
		
		mBallScale = Manager.mElementManager.BallScale;
	}
	
	public void Update()
	{
		updateZoom(0);
		mBallCamera.transform.LookAt(getLookAtPosition(),Manager.get_up_vector(mBall.transform.position));
		
		
		Vector3 dp = KeyboardUtilities.get_keyboard_as_vector();
		dp.y = -dp.y*2;
		updateHitRot(dp);
		dp.y = 0;
		dp.x *= 8;
		updateRot(dp);
		
		float defaultAngle = 65;
		if(Mathf.Abs(get_camera_angle()-defaultAngle) > 5)
		{
			updateRot(new Vector3(0,(get_camera_angle()-defaultAngle)*-0.05f,0));
		}
	}
	
	public void position_camera_at_default()
	{
	}
	
	float get_camera_angle()
	{
		Vector3 toCamera = (mBallCamera.transform.position - mBall.transform.position).normalized;
		float angle = Vector3.Angle(Manager.get_up_vector(mBall.transform.position),toCamera);
		return angle;
	}
	
	public Vector3 get_hit_direction()
	{
		Vector3 up = Manager.get_up_vector(mBall.transform.position);
		Vector3 dir = Vector3.Exclude(up,(mBall.transform.position - mBallCamera.transform.position));
		dir.Normalize();
		return up * Mathf.Sin(mHitAngle) + dir * Mathf.Cos (mHitAngle);
	}
	
	
	public void handle_pinch(float change)
	{
		updateZoom(change*0.01f);	
	}
	
	public bool handle_press(InputManager.MouseProfile mouse)
	{
		return true;
	}
	
	public void handle_mouse_down_motion(InputManager.MouseProfile mouse)
	{
		Vector3 dp = mouse.mPositions.get_last_value_difference();
		Vector3 npos = mouse.mPositions.get_change();
		float dt = mouse.mPositions.get_last_time_change();
		//updateHitRot(dp*500);
		dp.y = -dp.y;
		dp.y -= npos.y * 0.01f;
		dp.x += npos.x * 0.01f;
		updateRot(dp*500);
	}
	
	public void handle_release(InputManager.MouseProfile mouse)
	{
	}
	
	
	Vector3 get_ideal()
	{
		//TODO this should look towards the next closest scenic camera or goal I guess...
		//Vector3 lookDiff = Manager.mElementManager.ActiveGoal - Manager.mElementManager.
		return Vector3.zero;
	}
	
	void updateRot(Vector3 aChange)
    {
        float y = -aChange.y / 700.0f;
        float x = aChange.x / 700.0f;
        //Vector3 oldPos = mCamera.transform.position;
        //mBallCamera.transform.LookAt(mBall.transform,Manager.get_up_vector(mBall.transform.position));
        Vector3 targetPos = SpatialPosition.rotate_about(mBallCamera.transform.position, mBallCamera.transform.up, mBall.transform.position, y, x);
        Vector3 camForce = (targetPos -	 mBallCamera.transform.position) / 6.0f;
		
		if(!(get_camera_angle() < 25  && y > 0)) //don't let users go too high
        	mBallCamera.rigidbody.AddForce(camForce);
    }
	
     
    //this updates rotation of the hit angle object based on mouse input
    //TODO this needs to not allow for 360 degree rotation
    void updateHitRot(Vector3 aChange)
    {
        float y = -aChange.y / 400.0f;
        mHitAngle = Mathf.Clamp(mHitAngle + y, -Mathf.PI / 4, Mathf.PI);
    }

    Vector3 getLookAtPosition()
    {
        Vector3 up = Manager.get_up_vector(mBall.transform.position);
        float heightModifier = Mathf.Clamp(Mathf.Sqrt(Manager.get_scaled_distance_from_goal()) * 1.7f, 0, 1);
        return mBall.transform.position + up * heightModifier;
    }
	
	public void updateZoom(float aZoom)
    {
        mCamDistance += aZoom * (-100);
        mCamDistance = Mathf.Clamp(mCamDistance, mBallScale *2f, mBallScale * 7);
        Vector3 desired = mBall.transform.position + (mBallCamera.transform.position - mBall.transform.position).normalized * mCamDistance;
        Vector3 direction = desired - mBallCamera.transform.position;
        if (direction.magnitude > 0.1f)
            mBallCamera.rigidbody.AddForce((direction) * 0.01f);
    }
	
	public Vector3 getLaggedHitDirection()
	{
		Vector3 up = Manager.get_up_vector(mBall.transform.position);
		Vector3 hitDir =  mPreviousHitDirection.Peek();
		Vector3 arrowDir = up.normalized * Mathf.Sin(mHitAngle) + hitDir.normalized * Mathf.Cos(mHitAngle);
        return arrowDir;
	}
    public Vector3 getHitDirection()
    {
        Vector3 up = Manager.get_up_vector(mBall.transform.position);
        Vector3 hitDir = Vector3.Exclude(up, mBall.transform.position - mBallCamera.transform.position);
        Vector3 arrowDir = up.normalized * Mathf.Sin(mHitAngle) + hitDir.normalized * Mathf.Cos(mHitAngle);
        return arrowDir;
    }
	
	
	
	public void evt_game_state_changed()
	{
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_USER)
		{
			//TODO reset camera  position
			//mBallCamera.rigidbody.MovePosition()
		}
	}
}