using UnityEngine;
using System.Collections;

public class AnimalNoises : MonoBehaviour {
	AudioSource Source {get; set;}
	void Start () {
		Source = gameObject.AddComponent<AudioSource>();
		Source.loop = false;
	}
	public void play_animal_noise()
	{
		//TODO play a random animal noise
		if(!Source.isPlaying)
		{
			//lol lazy
			try{
				Source.clip = CentralManager.Manager.mRef.mSoundRef.mAnimalSounds[Random.Range(0,CentralManager.Manager.mRef.mSoundRef.mAnimalSounds.Length)];
			}catch{}
			Source.Play();
		}
	}
}
