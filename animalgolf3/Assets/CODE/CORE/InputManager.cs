using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	public class MultiMouseProfile
	{
		public System.Collections.Generic.Dictionary<uint,VectorSignal> mInputs = new System.Collections.Generic.Dictionary<uint, VectorSignal>();
		public void touch_down(uint id,Vector3 pos)
		{
			mInputs[id] = new VectorSignal();
			mInputs[id].add_absolute(pos,Time.time);
		}
		public VectorSignal touch_up(uint id)
		{
			if(!mInputs.ContainsKey(id))
				throw new UnityException("multimouse profile does not contain touch id " + id);
			VectorSignal r = mInputs[id];
			mInputs.Remove(id);
			return r;
		}
		public void touch_move(uint id,Vector3 pos)
		{
			if(!mInputs.ContainsKey(id))
				throw new UnityException("multimouse profile does not contain touch id " + id);
			mInputs[id].add_absolute(pos,Time.time);
		}
		
	}
	public class MouseProfile
	{
		public VectorSignal mPositions;
		public MouseProfile()
		{
			mPositions = new VectorSignal();//(to_relative(Input.mousePosition),Time.time);
		}
		
		public Vector3 get_last_mouse_position_relative(){return mPositions.get_last();}
		public Vector3 get_last_mouse_position_absolute(){return to_absolute(mPositions.get_last());}
		
		public static Vector3 to_absolute(Vector3 v)
		{
			Vector3 r = v;
			r.x *= Screen.width;
			r.y *= Screen.height;
			return r;
		}
		
		public static Vector3 to_relative(Vector3 v)
		{
			Vector3 r = v;
			r.x /= Screen.width;
			r.y /= Screen.height;
			return r;
		}
	};
	
	
	public class MouseEventHandler
	{
		public delegate bool  MouseHandlerDelegate(MouseProfile mouse);
		public delegate void VoidMouseHandlerDelegate(MouseProfile mouse);
		public delegate void PinchHandlerDelegate(float change);
		public MouseHandlerDelegate mMousePressed;
		public VoidMouseHandlerDelegate mMouseMoved;
		public VoidMouseHandlerDelegate mMouseReleased;
		public PinchHandlerDelegate mPinch;
		
		public bool mMouseDown = false;
		public float mTimeDown = 0;
		public MouseEventHandler(){ mMouseReleased += mouse_released; }
		public void mouse_pressed(){mMouseDown = true; mTimeDown = Time.time;}
		public float time_down(){return Time.time-mTimeDown;}
		public bool was_mouse_pressed(){ return mMouseDown; }
		void mouse_released(MouseProfile mouse){ mMouseDown = false; }
	}
	
	MouseProfile mMouse = new MouseProfile();
	
	
	public void handle_pinch(MouseEventHandler mHandler)
	{
		//TODO actually handle pinch
		float zoom = Input.GetAxis("Mouse ScrollWheel");
		mHandler.mPinch(zoom);
	}
	
	public bool handle_mouse(MouseEventHandler mHandler)
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(mHandler.mMousePressed(mMouse))
			{
				mHandler.mouse_pressed();
				return true;
			}
			return false;
		}
		if(!mHandler.was_mouse_pressed()) return false;
		mHandler.mMouseMoved(mMouse);
		if(Input.GetMouseButtonUp(0))
			mHandler.mMouseReleased(mMouse);
		return true;
	}
	public void Start () {
	}
	public void Update () {
		if(Input.GetMouseButtonDown(0))
			mMouse = new MouseProfile();
		if(Input.GetMouseButton(0))
			mMouse.mPositions.add_absolute(MouseProfile.to_relative(Input.mousePosition),Time.time);
		
		if(!handle_mouse(Manager.mInterfaceManager.mMouseHandler))
			if(!handle_mouse(Manager.mTargetManager.mMouseHandler))
				handle_mouse(Manager.mBallCameraManager.mMouseHandler);
		
		
		handle_pinch(Manager.mBallCameraManager.mMouseHandler);
	}
}
