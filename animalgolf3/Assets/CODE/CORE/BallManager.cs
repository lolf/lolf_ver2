using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	
	GameObject mBall;
	//for stabalizing
	public LinkedList<Vector3> mLastPosition = new LinkedList<Vector3>();
	QuTimer mTimeSinceShot = new QuTimer(0,1);
    float mOrigDrag;
    float mOrigAngularDrag;
	
	
	
	public void Start () {
		
		//TODO all ballso...
		mBall = Manager.Ball;
		foreach(Transform e in mBall.transform.GetComponentsInChildren<Transform>())
		{
			e.transform.gameObject.layer = Constants.BALL_LAYER;
		}
		mOrigDrag = mBall.rigidbody.drag;
        mOrigAngularDrag = mBall.rigidbody.angularDrag;
        mLastPosition.AddFirst(Vector3.zero * 1);
	}
	public void Update () {
		handleStabalization(Time.deltaTime);
		if(isStabilized(0.01f) && isExpired())
			Manager.trigger_event("ball_stabalized");
		if(!Manager.is_in_bounds(mBall.transform.position))
			Manager.mGameStateManager.set_state(GameStateManager.GameState.GS_OOB);
	}
	
	
	//rewrite me
    public void setTimer(float expire)
    {
        mTimeSinceShot.setTarget(expire);
    }

    public void resetMotionTimer()
    {
        mBall.rigidbody.drag = mOrigDrag;
        mBall.rigidbody.angularDrag = mOrigAngularDrag;
        mTimeSinceShot.reset();
    }
    public void handleStabalization(float delta)
    {
        mTimeSinceShot.update(delta);
        //TODO should use QuSignal type thing here to take care of the fact that fraamerate might not be uniform
        mLastPosition.AddFirst((mBall.transform.position)*1);
        if (mLastPosition.Count > 150)
            mLastPosition.RemoveLast();

        //TODO this number should change depending on whetehr we are in macro or micro mode and what level we are on
        //TODO make this easy to change ro soemtihng
        //TODO add a time factor in here as well
        
        if (mTimeSinceShot.getCurrent() > 1.5)
        {
			if(is_on_ground())
			{
	            float dp = computeAbsChange();
	            if (dp < 0.01f*Manager.mElementManager.BallScale)
	            {
					Debug.Log ("STAB");
	                if (dp > 0.05f*Manager.mElementManager.BallScale)
	                {
	                    mBall.rigidbody.drag = mBall.rigidbody.angularDrag = 10 / dp;
	                }
	                else mBall.rigidbody.drag = mBall.rigidbody.angularDrag = 10000;
	            }
			}
        }
    }

    public float computeAbsChange()
    {
        float r = 0;
        Vector3 p = mLastPosition.First.Value;
        foreach (Vector3 e in mLastPosition)
        {
            r += ((p - e).magnitude);
            p = e;
        }
        return r/mLastPosition.Count;
    }
    public bool isExpired()
    {
        return mTimeSinceShot.isExpired();
    }
    public bool isStabilized(float thresh = 0.05f)
    {
		//TODO should do net change in the last 1 second < a certain number to remove jiggling cases.
        return computeAbsChange() < thresh * Manager.mElementManager.BallScale;
        //return (mLastPosition - mBall.transform.position).magnitude < thresh;
    }
	
    //this one will reduce the stabilized parameter proportional to square root of time
    public bool isTimeBasedStabalized(float mult = 1)
    {
        if (!mTimeSinceShot.isExpired())
            return false;
        return isStabilized(0.01f + mult * (Mathf.Sqrt(mTimeSinceShot.getCurrent()) / 20.0f));
    }
	
	public bool is_on_ground()
	{
		RaycastHit hit;
		//these ones do not work because sweep test can't do mesh colliders...
		//if(mBall.rigidbody.SweepTest(-Manager.get_up_vector(mBall.transform.position),out hit,0.1f*Manager.mElementManager.BallScale))
		//if(mBall.rigidbody.SweepTest(-Manager.get_up_vector(mBall.transform.position),out hit,1))
		//TODO this should only check with ground type things... (i.e. not flying poo)
		if(Physics.CheckSphere(mBall.transform.position,Manager.mElementManager.BallScale/2))
		{
			//Debug.Log ("on ground");
			return true;
		}
		return false;
	}
	
	public void evt_ball_hit()
	{
		mBall.rigidbody.drag = mOrigDrag;
        mBall.rigidbody.angularDrag = mOrigAngularDrag;
		setTimer(1.5f);
		mTimeSinceShot.reset();
	}
	
	public void evt_ball_sunk()
	{
		Debug.Log ("ball sunk");
		foreach(Collider e in mBall.GetComponentsInChildren<Collider>())
			e.isTrigger = true;
		mBall.rigidbody.AddForce(Manager.get_up_vector(mBall.transform.position)*mBall.rigidbody.mass*Manager.mRef.mSettings.LEVEL_SCALE*2,ForceMode.Impulse);
	}
	
}