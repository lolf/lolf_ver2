using UnityEngine;
using System.Collections;

public class FollowMainCamera : MonoBehaviour {
	public GameObject mToMatch = null;
	
	void Update () {
        //transform.rotation = mToMatch.transform.rotation;
		if(mToMatch != null)
        	transform.rotation = Quaternion.Slerp(transform.rotation, mToMatch.transform.rotation, 0.1f);
		else 
			mToMatch = CentralManager.Manager.mCameraManager.Cam;	
	}
}
