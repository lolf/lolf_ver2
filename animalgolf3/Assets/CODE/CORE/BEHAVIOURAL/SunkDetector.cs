using UnityEngine;
using System.Collections;

public class SunkDetector : MonoBehaviour {
	ElementManager mManager;
	GameObject mBall;
	public void initialize(ElementManager aManager)
	{
		mManager = aManager;
	}
	void OnTriggerEnter(Collider other)
	{
		//TODO all balls
		if(other.attachedRigidbody == mManager.ActiveBall.rigidbody)
		{
			Debug.Log ("Sunk detected");
			CentralManager.Manager.trigger_event("ball_sunk");
		}
	}
}
