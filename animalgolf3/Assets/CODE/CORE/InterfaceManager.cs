using UnityEngine;
using System.Collections;

public class InterfaceManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	
	public InputManager.MouseEventHandler mMouseHandler = new InputManager.MouseEventHandler();
	
	//ClubSelector mClubs;
	GameObject mShotDisplay,mShotValue,mTimeDisplay,mTimeValue;
	AnimalFact mFacts;

	

	public void Start () 
	{
		mMouseHandler.mMousePressed += this.handle_press;
		mMouseHandler.mMouseMoved += this.handle_mouse_down_motion;
		mMouseHandler.mMouseReleased += this.handle_release;
		
		mFacts = new AnimalFact(Manager);
		//mClubs = new ClubSelector(Manager);
		
		mShotDisplay = new GameObject("genGuiTextContainer");
		mShotValue = new GameObject("genGuiTextContainer");
		mTimeDisplay = new GameObject("genGuiTextContainer");
		mTimeValue = new GameObject("genGuiTextContainer");
		
		mShotDisplay.AddComponent<GUIText>();
		mShotValue.AddComponent<GUIText>();
		mTimeDisplay.AddComponent<GUIText>();
		mTimeValue.AddComponent<GUIText>();
		
		GUIText gt = mShotDisplay.GetComponent<GUIText>();
		gt.text = "SHOTS";
		gt.font = Manager.mRef.mMaterialRef.mScoreFont;
		gt.fontSize = 20;
		gt.anchor = TextAnchor.UpperRight;
		gt.transform.position = new Vector3(1,1,0);
		gt = mShotValue.GetComponent<GUIText>();
		gt.text = "0";
		gt.font = Manager.mRef.mMaterialRef.mScoreFont;
		gt.fontSize = 15;
		gt.anchor = TextAnchor.UpperRight;
		gt.transform.position = new Vector3(1,0.95f,0);
		gt = mTimeDisplay.GetComponent<GUIText>();
		gt.text = "TIME";
		gt.font = Manager.mRef.mMaterialRef.mScoreFont;
		gt.fontSize = 20;
		gt.anchor = TextAnchor.UpperRight;
		gt.transform.position = new Vector3(1,0.9f,0);
		gt = mTimeValue.GetComponent<GUIText>();
		gt.text = "53212";
		gt.font = Manager.mRef.mMaterialRef.mScoreFont;
		gt.fontSize = 15;
		gt.anchor = TextAnchor.UpperRight;
		gt.transform.position = new Vector3(1,0.85f,0);
		
	}
	
	public void Update () 
	{
		//mClubs.Update();
		GUIText gt = mTimeValue.GetComponent<GUIText>();
		gt.text = System.String.Format("{0:F2}",Manager.mGameStateManager.time_since_begin());
		gt = mShotValue.GetComponent<GUIText>();
		gt.text = Manager.mTargetManager.mShotCount.ToString();
	}
	
	public bool handle_press(InputManager.MouseProfile mouse)
	{
		//return mClubs.handle_mouse_press(mouse);
		return false;
	}
	
	public void handle_mouse_down_motion(InputManager.MouseProfile mouse)
	{
		
	}
	
	public void handle_release(InputManager.MouseProfile mouse)
	{
	}
	
	public void evt_game_state_changed()
	{
		if(mFacts != null)
			mFacts.destroy_facts();
	}
	
}
