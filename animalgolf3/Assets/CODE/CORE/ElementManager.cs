using UnityEngine;
using System.Collections.Generic;

public class ElementManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	public List<GameObject> Balls { get; private set; }
	public List<Collider> Goals { get; private set; }
	public GameObject StageParent {get; private set; }
	
	public GameObject ActiveBall
	{
		get; private set;
	}
	public float BallScale
	{
		get{
			return ActiveBall.union_mesh_bounds().extents.magnitude;
		}
	}
	public float BallMass
	{
		get{
			return ActiveBall.rigidbody.mass;
		}
	}
	public Collider ActiveGoal
	{
		get; private set;
	}
		
	
	//TODO should not be awake
	public void Awake () {
		Balls = new List<GameObject>();
		Goals = new List<Collider>();
		
		//find all goals
		//TODO set layers
		GameObject findball = GameObject.Find("ST_BALL");
		if(findball != null)
			Balls.Add(findball);
		for(int i = 0; i < 30; i++)
		{
			GameObject findballs = GameObject.Find("ST_BALL_"+i);
			if(findballs != null)
				Balls.Add(findballs);
			else break;
		}
		if(Balls.Count > 0)
			ActiveBall = Balls[0];
		else throw new UnityException("no balls found");
		
		
		
		//find all goals
		GameObject findgoal = GameObject.Find("ST_GOAL");
		if(findgoal != null && findgoal.GetComponent<Collider>() != null)
			Goals.Add(findgoal.GetComponent<Collider>());
		for(int i = 0; i < 30; i++)
		{
			GameObject findgoals = GameObject.Find("ST_GOAL_"+i);
			if(findgoals != null && findgoals.GetComponent<Collider>() != null)
				Goals.Add(findgoals.GetComponent<Collider>());
			else break;
		}
		
		if(Goals.Count > 0)
			ActiveGoal = Goals[0];
		else throw new UnityException("no goals found");
		
		
		foreach(Collider e in Goals)
		{
			e.gameObject.AddComponent<SunkDetector>().initialize(this);
		}
		initialize_default_settings(Manager);
		
		//find stage
		StageParent = GameObject.Find("ST_STAGE");
		if(StageParent == null)
			throw new UnityException("no stage found");
	}
	
	
	
	
	//TODO cleanup...
	public GameObject[] mCameraRelays = null;
	public GameObject mIntroCameraPosition = null;
	public GameObject mStartingCameraPosition = null;
	public GameObject mOverheadCameraPosition = null;
	public GameObject mStageCenter = null; //not required
	
	public void initialize_default_settings(CentralManager aManager)
	{
		mStartingCameraPosition = GameObject.Find("ST_CAMERA_START");
		mIntroCameraPosition = GameObject.Find("ST_INTRO_CAMERA");
		mStageCenter = GameObject.Find("ST_STAGE_CENTER");
		mOverheadCameraPosition = GameObject.Find ("ST_CAMERA_OVERHEAD");
		GameObject rcp = GameObject.Find("ST_RELAY_PARENT");
		if(rcp != null)
		{
			mCameraRelays = new GameObject[rcp.transform.childCount];
			for(int i = 0; i < rcp.transform.childCount; i++)
			{
				mCameraRelays[i] = rcp.transform.FindChild((i+1).ToString()).gameObject;
				if(mCameraRelays[i] == null)  //dud children
					break;
			}
		}
		
		
		//set some defaults
		if(mStageCenter == null)
		{
			mStageCenter = new GameObject("genCenter");
			mStageCenter.transform.position = (ActiveBall.transform.position + ActiveGoal.transform.position)/2.0f;
		}
		
		if(mCameraRelays == null || mCameraRelays.Length == 0)
		{
			mCameraRelays = new GameObject[1];
			mCameraRelays[0] = new GameObject("genRelay");
			mCameraRelays[0].transform.position = mStageCenter.transform.position + aManager.get_up_vector(mStageCenter.transform.position)*BallScale*5;
		}
		if(mStartingCameraPosition == null)
		{
			mStartingCameraPosition = new GameObject("genStartCameraPosition");
			Vector3 gnd = ActiveBall.transform.position*0.75f + ActiveGoal.transform.position*0.25f;
			//NOTE, strictly speaking, we are not guaranteed that getUpVector is set to what we want it to be due to execution order uncertainty but we don't really care
			mStartingCameraPosition.transform.position = gnd + aManager.get_up_vector(gnd)*(ActiveBall.transform.position-gnd).magnitude;
		}
		if(mIntroCameraPosition == null)
			mIntroCameraPosition = mStartingCameraPosition;
		if(mOverheadCameraPosition == null)
		{
			mOverheadCameraPosition = new GameObject("genOverheadCameraPosition");
			mOverheadCameraPosition.transform.position = mStageCenter.transform.position + aManager.get_up_vector(mStageCenter.transform.position) * (ActiveBall.transform.position - ActiveGoal.transform.position).magnitude;
			mOverheadCameraPosition.transform.LookAt(mStageCenter.transform.position);
		}
		
	}
}
