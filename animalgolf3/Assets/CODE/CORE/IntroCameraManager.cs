using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IntroCameraManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	public InputManager.MouseEventHandler mMouseHandler = new InputManager.MouseEventHandler();
	
	public GameObject mIntroCamera;
	
	public QuTimer mTimer = new QuTimer(0,0);//new QuTimer(0,1.5f);
	public void Start () {
		mIntroCamera = new GameObject("genIntroCamera");
		//TODO
		mIntroCamera.transform.position = Manager.mElementManager.mIntroCameraPosition.transform.position;
		mIntroCamera.transform.LookAt(Manager.Ball.transform);
	}
	
	public void Update() {
		//TODO do fancy camera stuff here
		//TODO
		mIntroCamera.transform.LookAt(Vector3.zero);
		mTimer.update(Time.deltaTime);
		//you should really use a callback on the timer here instead.
		if(mTimer.isExpired() && Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_PREVIEW)
			Manager.mGameStateManager.set_state(GameStateManager.GameState.GS_USER);
	}
	
	
	void update_intro_camera()
	{
		/*
		List<Vector3> lines = new List<Vector3>();
		foreach(GameObject e in Manager.mRef.mHookups.mCameraRelays)
			lines.Add(e.transform.position);
		Vector3 minPosition = lines[0];
		for(int i = 0; i < lines.Count - 1; i++)
		{
			Vector3 np = VectorMathUtilities.closest_point_on_segment(lines[i],lines[i+1],Manager.Ball.transform.position);
			if((np-mBall.transform.position).sqrMagnitude < (minPosition-Manager.Ball.transform.position).sqrMagnitude)
				minPosition = np;
		}*/
		//TODO finish
	}
}
