using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class CentralManager : MonoBehaviour {
	public static CentralManager Manager
	{
		get; set;
	}
	
	//references
	public class AnimalLevelReferences
	{
		public AnimalLevelReferences(CentralManager aManager)
		{
			mPrefabRef = aManager.transform.parent.GetComponentInChildren<PrefabReferences>();
			mSoundRef = aManager.transform.parent.GetComponentInChildren<SoundReferences>();
			mMaterialRef = aManager.transform.parent.GetComponentInChildren<MaterialReferences>();
			mSettings = aManager.transform.parent.GetComponent<LevelSettings>();
			
		}
		
		public PrefabReferences mPrefabRef;
		public MaterialReferences mMaterialRef;
		public SoundReferences mSoundRef;
		public LevelSettings mSettings;
	};
	
	public AnimalLevelReferences mRef;
	public GameObject Ball {get{return mElementManager.ActiveBall;}}
	
	public GUIDump Dump {get; set;}
	
	TimedEventDistributor TED = new TimedEventDistributor();
	
	
	//managers
	public ElementManager mElementManager;
	public ScenicCameraManager mScenicCameraManager;
	public PredictorManager mPreditorManager; 
	public IntroCameraManager mIntroCameraManager;
	public BallCameraManager mBallCameraManager;
	public InputManager mInputManager;

	public CameraManager mCameraManager;
	public GameStateManager mGameStateManager;
	public InterfaceManager mInterfaceManager;
	public TargetManager mTargetManager;
	public BallManager mBallManager;
	
	void Awake () {
		//if(Manager == null) we actually don't want to check so this variable for sure gets reset when a new scene is loaded
			Manager = this;
		
		Physics.gravity = new Vector3(0,-9.8f,0);
		
		
		
		mRef = new AnimalLevelReferences(this);
		
		//set up gui dump keke
		GameObject dumpObj = new GameObject("gen_GUIDump");
		dumpObj.AddComponent<Camera>().cullingMask = 0;
		dumpObj.camera.clearFlags = CameraClearFlags.Depth;
		Dump = dumpObj.AddComponent<GUIDump>();
		Dump.dump += debug_gui;
		
		//create our fake monobehaviour scripts here
		mElementManager = gameObject.AddComponent<ElementManager>();
		mScenicCameraManager = gameObject.AddComponent<ScenicCameraManager>();
		mPreditorManager = gameObject.AddComponent<PredictorManager>();
		mIntroCameraManager = gameObject.AddComponent<IntroCameraManager>();
		mBallCameraManager = gameObject.AddComponent<BallCameraManager>();
		mInputManager = gameObject.AddComponent<InputManager>();
		mCameraManager = gameObject.AddComponent<CameraManager>();
		mGameStateManager = gameObject.AddComponent<GameStateManager>();
		mInterfaceManager = gameObject.AddComponent<InterfaceManager>();
		mTargetManager = gameObject.AddComponent<TargetManager>();
		mBallManager = gameObject.AddComponent<BallManager>();
		
		mRef.mSettings.initialize_default_settings(this);
	}
	
	void debug_gui()
	{
		//GUI.Box(new Rect(0,0,200,50),mElementManager.
	}
		
	void Update () {
		TED.update(Time.deltaTime);
		
		if(Input.GetKeyDown(KeyCode.Alpha2))
		{
        	restart_level();
		}
		if(Input.GetKeyDown (KeyCode.Alpha3))
		{
			next_level();
		}
		if(Input.GetKeyDown (KeyCode.Escape))
		{
			Application.Quit();
		}
	}
	
	void FixedUpdate() {
		
	}
	
	public void restart_level()
	{
		Application.LoadLevel(Application.loadedLevelName);
	}
	
	public void next_level()
	{
		Application.LoadLevel((Application.loadedLevel + 1)%Application.levelCount);
	}
	
	static bool defaultInBounds(Vector3 aWorldPosition){return aWorldPosition.y > -10;}
	static Vector3 defaultUpVector(Vector3 aWorldPosition) { return Vector3.up; }
    static Vector3 defaultGravityVector(Vector3 aWorldPosotion) { return Physics.gravity; }
	public Func<Vector3,bool> is_in_bounds = new Func<Vector3,bool>(defaultInBounds);
    public Func<Vector3,Vector3> get_up_vector = new Func<Vector3,Vector3>(defaultUpVector);
    public Func<Vector3,Vector3> get_gravity_vector = new Func<Vector3,Vector3>(defaultGravityVector);
	
	
    public float get_scaled_distance(Vector3 aPos1, Vector3 aPos2)
    {
        return (aPos1-aPos2).magnitude/mRef.mSettings.LEVEL_SCALE;
    }
	
	public float get_distance_from_goal()
	{
		return (mElementManager.ActiveBall.transform.position - mElementManager.ActiveGoal.transform.position).magnitude;
	}
	
    public float get_scaled_distance_from_goal()
    {
        return get_scaled_distance(mElementManager.ActiveBall.transform.position, mElementManager.ActiveGoal.transform.position);
    }
	
	public void trigger_event(string name)
	{
		Debug.Log ("triggering event " + name);
		gameObject.SendMessage("evt_"+name,null,SendMessageOptions.DontRequireReceiver);
	}
	
	//TODO delete
	public void evt_ball_sunk()
	{
		Debug.Log ("go to next level");
		TED.add_one_shot_event(
			next_level
		,4);
	}
	
	public void evt_game_state_changed()
	{
		if(mGameStateManager.get_state() == GameStateManager.GameState.GS_OOB)
		{
			restart_level();
		}
	}
}
