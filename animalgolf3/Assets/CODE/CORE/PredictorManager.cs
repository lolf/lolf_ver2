using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class PredictorManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}

	GameObject mBall;
	public GameObject mParentArrow;
	public List<GameObject> mArrows = new List<GameObject>();
	public void Start () {
		mBall = Manager.Ball;
		mParentArrow = new GameObject("genParentArrow");
		create_arrows();
	}
	
	public void Update() {
		float ballScale = Manager.mElementManager.BallScale;
		float ballMass = Manager.mElementManager.BallMass;
		float ballDrag = 0.1f;
		Vector3 dir = Manager.mBallCameraManager.get_hit_direction();
		Vector3 ballVel = Manager.mTargetManager.get_hit_impulse()/ballMass * dir;
		float stepTime = 0.05f;
		
		
		Vector3 start = Manager.mTargetManager.get_target_position() + dir*ballScale*0.5f;
		for(int i = 0; i < mArrows.Count; i++)
		{
			
			//dragForceMagnitude = velocity.magnitude ^ 2 * drag; // The variable you're talking about
			//dragForceVector = dragForceMagnitude * -velocity.normalized;
			
			//TODO BE :D
			//TODO check for collision and break
			//TODO drag??
			mArrows[i].transform.position = start;
			Vector3 gravity = Manager.get_gravity_vector(start);
			start += ballVel*stepTime;
			ballVel += gravity * stepTime;
			mArrows[i].transform.LookAt(start - ballVel);
			
		}
		
		//TODO do fancy arrows eventually
	}
	
	public void create_arrows()
	{
		foreach(GameObject e in mArrows)
			GameObject.DestroyImmediate(e);
		mArrows.Clear();
		float ballScale = Manager.mElementManager.BallScale;
		for(int i = 0; i < 20; i++)
		{
			var arrow = (GameObject)GameObject.Instantiate(Manager.mRef.mPrefabRef.mPredictorArrows);
			arrow.transform.localScale = new Vector3(1,1,1)*ballScale*0.2f;
			mArrows.Add(arrow);
			mArrows[mArrows.Count-1].transform.parent = mParentArrow.transform;
		}
	}
	void hide_arrows(bool hide)
	{
		foreach(GameObject e in mArrows)
			e.renderer.enabled = !hide;
	}
	
	public void evt_game_state_changed()
	{
		if(Manager.mGameStateManager.get_state() == GameStateManager.GameState.GS_USER)
			hide_arrows(false);
		else hide_arrows(true);
	}
}