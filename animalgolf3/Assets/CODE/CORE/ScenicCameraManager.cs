using UnityEngine;
using System.Collections;
public class ScenicCameraManager : MonoBehaviour, FakeMonoBehaviour {
	public CentralManager Manager {get{return CentralManager.Manager;}}
	public GameObject mBall;
	public GameObject mScenicCamera;
	public void Start () 
	{
		mBall = Manager.Ball;
		mScenicCamera = new GameObject("genScenicCamera");
		mScenicCamera.transform.position = Manager.mElementManager.mCameraRelays[0].transform.position;
	}
	
	float get_lambda(Vector3 start, Vector3 end, Vector3 pt)
	{
		return Mathf.Clamp01(Vector3.Dot(pt,end-start)/(end-start).sqrMagnitude);
	}
	Vector3 get_closest_point_on_line(Vector3 start, Vector3 end, Vector3 pt)
	{
		float lambda = get_lambda(start,end,pt);
		return (1-lambda)*start + lambda*end;
	}
	float get_distance_to_line(Vector3 start, Vector3 end, Vector3 pt)
	{
		return (pt-get_closest_point_on_line(start,end,pt)).sqrMagnitude;
	}
	
	public void Update()
	{
		//interpolating vernsion
		float min = Mathf.Infinity;
		Vector3 closest = Manager.mElementManager.mCameraRelays[0].transform.position;
		for(int i = 1; i < Manager.mElementManager.mCameraRelays.Length; i++)
		{
			Vector3 tc = get_closest_point_on_line(Manager.mElementManager.mCameraRelays[i-1].transform.position,Manager.mElementManager.mCameraRelays[i].transform.position,mBall.transform.position);
			if(min > (tc-mBall.transform.position).sqrMagnitude)
			{
				min = (tc-mBall.transform.position).sqrMagnitude;
				closest = tc;
			}
		}
		//lame no interpolation version, probbaly better anywaysnt
		/*
		float min = Mathf.Infinity;
		Vector3 closest = Manager.mElementManager.mCameraRelays[0].transform.position;
		for(int i = 0; i < Manager.mElementManager.mCameraRelays.Length; i++)
		{
			float tc = (Manager.mElementManager.mCameraRelays[i].transform.position-mBall.transform.position).sqrMagnitude;
			if(min > tc)
			{
				min = tc;
				closest = Manager.mElementManager.mCameraRelays[i].transform.position;
			}
		}
		*/
		mScenicCamera.transform.position = closest;
		mScenicCamera.transform.LookAt(mBall.transform,Manager.get_up_vector(mScenicCamera.transform.position));
	}
}