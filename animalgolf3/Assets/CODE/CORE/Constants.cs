using UnityEngine;
using System.Collections;

public struct Constants
{
	public const int BALL_LAYER = 8;
	public const int INTERNAL_TARGET_LAYER = 9;
}
