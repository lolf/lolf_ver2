using UnityEngine;
using System.Collections;

public class ObjectAccessingScript : MonoBehaviour {
    public GameObject temp1;
    public int param1 = 9;
	// Use this for initialization
	void Start () {
        param1 = 10;
        temp1 = GameObject.FindGameObjectWithTag("BALL");
	}
	
	// Update is called once per frame
	void Update () {
        temp1.transform.position = temp1.transform.position + new Vector3(1, 0, 0);
	}
}
