using UnityEngine;
using System.Collections;
//OBSOLETE
//TODO delete me
public class BallWhack : MonoBehaviour {
    GameObject mBall;
    Camera mCamera;
    LevelSettings mSettings;

    AnimalMouseManager mLeftMouse = new AnimalMouseManager();
    AnimalMouseManager mRightMouse = new AnimalMouseManager();

    AnimalTarget mAnimalTarget = new AnimalTarget();
    AnimalCamPhys mCamPhys = new AnimalCamPhys();
    AnimalMotionTimer mTimer = new AnimalMotionTimer();

    GameObject mHitEmit;

    bool mSetCursorPositionMode = false;
    RaycastHit mLastHit;

	// Use this for initialization
	void Start () 
    {
        mHitEmit = GameObject.FindGameObjectWithTag("EFFECTS").GetComponent<EffectsList>().fx[0];//.GetComponent<ParticleEmitter>();
        mSettings = GetComponent<LevelSettings>();
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();

        Physics.gravity = mSettings.GRAVITY;
        RenderSettings.fog = mSettings.USE_FOG;
        mAnimalTarget.initialize();
        mCamPhys.initialize();
        //TODO 
        //mTimer.initialize();
	}

    bool mouseCast(int layer, out RaycastHit hit)
    {
        Vector3 mouse = Input.mousePosition;
        mouse.x /= (float)Screen.width;
        mouse.y /= (float)Screen.height;
        Ray toCast = mCamera.ViewportPointToRay(mouse);
        return Physics.Raycast(toCast, out hit, Mathf.Infinity, layer);
    }

    bool setCursorPosition()
    {
        if (mouseCast(1 << 8, out mLastHit))
        {
            mAnimalTarget.setTargetPosition(mLastHit.point, mLastHit.normal);
            return true;
        }
        return false;
    }
	// Update is called once per frame
	void Update () 
    {
        //hit the ball
        bool canHit = mTimer.isStabilized();
        mTimer.update(Time.deltaTime);
        if (Input.GetMouseButtonUp(0))
        {
            if (mSetCursorPositionMode == true)
                mSetCursorPositionMode = false;
            else
            {
                if (canHit && mAnimalTarget.isSet)
                {
                    //float t = mRightMouse.mouseReleased(Input.mousePosition);
                    mRightMouse.mouseReleased(Input.mousePosition);
                    float t = 0.1f;
                    float d = mRightMouse.getMouseChange().magnitude;
                    if (d > 10 && t < 1)
                    {
                        float force = Mathf.Pow(d, 5 / 3.0f) / Mathf.Pow(t, 5 / 4.0f) / 400.0f;
                        mBall.rigidbody.AddForceAtPosition(mAnimalTarget.getHitDirection() * force, mAnimalTarget.getHitPosition(), ForceMode.Impulse);
                        mHitEmit.transform.position = mAnimalTarget.getHitPosition();
                        //mHitEmit.SendMessage("Emit",30);// Emit((int)(force / 100.0f));
                        mHitEmit.SendMessage("Emit", (int)(force / 100.0f));
                    }
                }
            }
        }

        //set the arrow
        if (Input.GetMouseButtonDown(0))
        {
            mRightMouse.mousePressed(Input.mousePosition);
            if (setCursorPosition())
                mSetCursorPositionMode = true;
        }
        if(mSetCursorPositionMode)
        {
            mRightMouse.mouseMoved(Input.mousePosition);
            mAnimalTarget.rotate(mRightMouse.getMouseChange());
        }

        //set the camera
        if (Input.GetMouseButtonDown(1))
            mLeftMouse.mousePressed(Input.mousePosition);
        if (Input.GetMouseButtonUp(1))
            mLeftMouse.mouseReleased(Input.mousePosition);
        else if (mLeftMouse.isCursorDown)
        {
            Vector3 pos = mLeftMouse.getMouseChange();
            mLeftMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mLeftMouse.getMouseChange();
            Vector3 dp = npos - pos;
            mCamPhys.updateRot(dp);
        }

        

        //oldPos = mCamera.transform.position;
        float zoom = Input.GetAxis("Mouse ScrollWheel");
        mCamPhys.updateZoom(zoom);
        mCamPhys.update();
	}

}
