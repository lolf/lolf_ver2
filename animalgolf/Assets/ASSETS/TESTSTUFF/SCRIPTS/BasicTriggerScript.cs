using UnityEngine;

using System.Collections;

public class BasicTriggerScript : MonoBehaviour
{
    GameObject mBall;
    GameObject mGameCamera;
    LevelSettings mSettings;
    AudioSource mSource;
    AnimalTimer mEventTimer = new AnimalTimer(0, 6.0f);
    bool mIn = false;
    void Start()
    {
        mSettings = GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<LevelSettings>();

        mBall = GameObject.FindGameObjectWithTag("BALL");
        mGameCamera = GameObject.FindGameObjectWithTag("CAMERA");
        
        mSource = gameObject.AddComponent<AudioSource>();

    }

	void Update()
	{
		if(mIn)
		{
            mEventTimer.update(Time.deltaTime);
			
			if (mEventTimer.hasTriggered(0.75f))
				mSource.PlayOneShot(mSettings.SUCCESS_SOUND);
            if (mEventTimer.hasTriggered(2))
            {
                Debug.Log("launch++");
                Vector3 up = mSettings.getUpVector(mBall.transform.position);
                mBall.rigidbody.AddForce(up * 5000 * mBall.rigidbody.mass , ForceMode.Impulse);
                mGameCamera.rigidbody.AddForce(up * 10000 * mGameCamera.rigidbody.mass / mBall.rigidbody.mass, ForceMode.Impulse);
            }
			if(mEventTimer.isExpired())
				Application.LoadLevel(mSettings.NEXT_SCENE);	
		}
	}
	
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.IsChildOf(mBall.gameObject.transform))
        {
        	if(!mIn)
        	{
        		mSource.PlayOneShot(mSettings.SINK_SOUND);
                GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<AnimalBackgroundMusic>().fadeOutPermanently();
        	}
        	mIn = true;
            //Application.LoadLevel(mSettings.NEXT_SCENE);
        }
    }
    
}
