using UnityEngine;
using System.Collections;

public class FogChanger : MonoBehaviour 
{
    bool mFog = true;
    public void OnWillRenderObject()
    {
        mFog = RenderSettings.fog;
        RenderSettings.fog = false;

    }
    public void OnRenderObject()
    {
        RenderSettings.fog = false;
    }
}
