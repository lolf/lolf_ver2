using UnityEngine;
using System.Collections;
//OBSOLETE
public class AnimalTarget {
    GameObject mArrow;
    GameObject mArrowStart;
    GameObject mArrowTarget;
    public GameObject mBall;
    protected Camera mCamera;
    public bool isSet = false;
    public float mRange = 6;

    public void initialize()
    {
        isSet = false;
        mArrow = GameObject.FindGameObjectWithTag("ARROW");
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();
        mArrowTarget = new GameObject("GenArrowTarget");
        mArrowStart = new GameObject("GenArrowStart");
        mArrow.transform.parent = mBall.transform;
        mArrowTarget.transform.parent = mBall.transform;
        mArrowStart.transform.parent = mBall.transform;
    }
    public void rotate(Vector3 aChange)
    {
        float y = Mathf.Clamp(aChange.y / 500.0f, -mRange, mRange);
        float x = Mathf.Clamp(-aChange.x / 500.0f, -mRange, mRange);
        mArrow.transform.position = AnimalMath.rotateAbout(mArrowStart.transform.position, mArrowStart.transform.up, mArrowTarget.transform.position, y, x);
        mArrow.transform.LookAt(mArrowTarget.transform.position);
    }
    public void setTargetPosition(Vector3 aPos, Vector3 aNorm)
    {
        isSet = true;
        //mArrow.transform.position = aPos + aNorm * 5;
        mArrow.transform.position = aPos + (-aPos + mCamera.transform.position).normalized * 5;
        mArrowStart.transform.position = mArrow.transform.position;
        mArrowTarget.transform.position = aPos;
        mArrow.transform.LookAt(mArrowTarget.transform);
        mArrowStart.transform.LookAt(mArrowTarget.transform);
    }
    public Vector3 getHitDirection()
    {
        return mArrow.transform.forward;
    }
    public Vector3 getHitPosition()
    {
        return mArrow.transform.position;
    }
}
