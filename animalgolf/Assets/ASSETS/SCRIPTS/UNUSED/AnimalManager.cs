using UnityEngine;
using System.Collections;
 
public class AnimalManager : MonoBehaviour {
    GameObject mBall;
    Camera mCamera;

    LevelSettings mSettings;
    public LevelSettings getSettings() { return mSettings; }

    AnimalMouseManager mLeftMouse = new AnimalMouseManager();
    AnimalMouseManager mRightMouse = new AnimalMouseManager();

    AnimalMicroTarget mAnimalTarget = new AnimalMicroTarget();
    AnimalCamPhys mCamPhys = new AnimalCamPhys();
    AnimalMotionTimer mAnimalMotionTimer = new AnimalMotionTimer();

    GameObject mHitEmit;
    AudioSource mSource;

    bool mSetCursorPositionMode = false;
    RaycastHit mLastHit;
    bool mCanHit = false;

    // Use this for initialization
    void Start()
    {
        mSettings = GetComponent<LevelSettings>();

        mBall = GameObject.FindGameObjectWithTag("BALL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();

        mAnimalTarget.initialize();
        mCamPhys.initialize();
        mAnimalMotionTimer.initialize();

        tempStart();
    }

    //contains stuff we should farm out...
    void tempStart()
    {
        mHitEmit = GameObject.FindGameObjectWithTag("EFFECTS").GetComponent<EffectsList>().fx[0];//.GetComponent<ParticleEmitter>();
        mSource = mBall.AddComponent<AudioSource>();
        mSource.volume = 1;
        mSource.clip = mSettings.HIT_SOUND;
    }



    //TODO WRITE A FUCKING INPUT MANAGER ARRRRG
    public void Update()
    {

        BaseInputRoutine();

        AnimalHitInputRoutine();

        //ArrowStretchRoutine(); no longer used/needed DELETE

        CanHitAndHideArrowRoutine();

        FollowBallCameraRoutine();
    }


    void BaseInputRoutine()
    {

        //INPUT ROUTINE
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.LoadLevel(Application.loadedLevelName);
        if (Input.GetKeyDown(KeyCode.F1))
            Application.LoadLevel(mSettings.NEXT_SCENE);
    }

    void AnimalHitInputRoutine()
    {
        //TODO clean this up
        //apply a fource if needed
        if (Input.GetMouseButtonUp(0))
        {
            //TODO get rid of isSet and make it raycast from camera  and give a default position
            if (mCanHit && !mAnimalTarget.isTargetting())
            {
                //float t = mRightMouse.mouseReleased(Input.mousePosition);
                mLeftMouse.mouseReleased(Input.mousePosition);
                float d = mLeftMouse.getMouseChange().magnitude;
                Debug.Log(mAnimalTarget.mArrowStretch);
                if (mAnimalTarget.mArrowStretch > 0.2f)
                {
                    //we can't hit anymore, reset the hit timer
                    mCanHit = false;
                    mAnimalMotionTimer.resetMotionTimer();

                    float force = Mathf.Pow(d, 5 / 3.0f) / Mathf.Pow(0.1f, 5 / 4.0f) / 400.0f;
                    Debug.Log("whack " + mAnimalTarget.mArrowStretch);

                    //mBall.rigidbody.AddForceAtPosition(mAnimalTarget.getHitDirection() * force, mAnimalTarget.getHitPosition(), ForceMode.Impulse);

                    mBall.rigidbody.AddForceAtPosition(mAnimalTarget.getHitDirection() * Mathf.Pow(mAnimalTarget.mArrowStretch * 10 + 4, 1.6666f) * 30f * mSettings.LEVEL_SCALE, mAnimalTarget.getHitPosition(), ForceMode.Impulse);
                    mHitEmit.transform.position = mAnimalTarget.getHitPosition();
                    //mHitEmit.SendMessage("Emit",30);// Emit((int)(force / 100.0f));
                    mHitEmit.SendMessage("Emit", (int)(force / 100.0f));

                    //TODO play a sound
                    mSource.PlayOneShot(mSettings.HIT_SOUND);
                }
                else
                {
                    mSource.PlayOneShot(mSettings.HIT_HARDER_SOUND);
                }
            }
        }
    }

    //DELETE
    void ArrowStretchRoutine()
    {
        //stretch the arrow when dragged.
        if (Input.GetMouseButtonDown(0))
            mLeftMouse.mousePressed(Input.mousePosition);
        if (Input.GetMouseButtonUp(0))
            mLeftMouse.mouseReleased(Input.mousePosition);
        mAnimalTarget.mArrowStretch = 0f;
    	if (Input.GetMouseButton(0) && mCanHit && !mAnimalTarget.isTargetting())
        {
            Vector3 pos = mLeftMouse.getMouseChange();
            mLeftMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mLeftMouse.getMouseChange();
            float arrowStretchStiffness = .75f;
            mAnimalTarget.mArrowStretch = .01f * Mathf.Pow(Mathf.Clamp(npos.magnitude,0,1100), arrowStretchStiffness);//npos.magnitude;//Input.mousePosition.magnitude;
            mAnimalTarget.update();
        }
    }

    void CanHitAndHideArrowRoutine()
    {
        //determine if we can hit or not
        bool canHit = mAnimalMotionTimer.isTimeBasedStabalized();
        mAnimalMotionTimer.update(Time.deltaTime);
        if (!mCanHit && canHit)
        {
            //mAnimalTarget.noMouseSetCursorPosition();
            if(!mAnimalTarget.isSet)
                mAnimalTarget.noMouseSetCursorPosition();
            mCanHit = true;
            mSource.PlayOneShot(mSettings.CAN_HIT_SOUND);
            Debug.Log("can hit");
        }
        if (mCanHit)
        {
            mAnimalTarget.update();
            mAnimalTarget.hideArrow(false);
        }
        else
        {
            mAnimalTarget.hideArrow(true);
        }
    }

    void FollowBallCameraRoutine()
    {
        //TODO separate this into its own class
        //set the camera
        if (Input.GetMouseButtonDown(1))
        {
            mRightMouse.mousePressed(Input.mousePosition);

            Vector3 pos = mRightMouse.getMouseChange();
            mRightMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mRightMouse.getMouseChange();
            Vector3 dp = npos - pos;
        }
        if (Input.GetMouseButtonUp(1))
            mRightMouse.mouseReleased(Input.mousePosition);
        else if (Input.GetMouseButton(1))
        {
            Vector3 pos = mRightMouse.getMouseChange();
            mRightMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mRightMouse.getMouseChange();
            Vector3 dp = npos - pos;
            mCamPhys.updateRot(dp);

        }
        float zoom = Input.GetAxis("Mouse ScrollWheel");
        mCamPhys.updateZoom(zoom);
        mCamPhys.update();
    }
}
