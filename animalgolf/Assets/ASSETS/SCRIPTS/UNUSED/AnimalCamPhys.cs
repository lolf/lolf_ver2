using UnityEngine;
using System.Collections;

public class AnimalCamPhys{
    Camera mCamera;
    GameObject mBall;
    GameObject mSight;
    GameObject mHitAngleObject;

    AnimalTimer mTimer;
    float mCamDistance;

    public void initialize()
    {
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mSight = new GameObject("genSightObject");
        //TODO mSight should be pulled from the targetting script
        mSight.transform.parent = mBall.transform;
        mSight.transform.localPosition = Vector3.zero;
        mHitAngleObject = new GameObject("genHitAngleObject");
      
        mCamera.gameObject.AddComponent<Rigidbody>();
        mCamera.gameObject.AddComponent<SphereCollider>().radius = 10;
        //mCamera.rigidbody.freezeRotation = true;
        mCamera.rigidbody.useGravity = false;
        mCamera.rigidbody.drag = 3;
        mCamera.rigidbody.mass = 0.001f;

        //match the hit angle object parameters to the camera parmeters
        mHitAngleObject.AddComponent<Rigidbody>().mass = 0.001f;
        mHitAngleObject.rigidbody.drag = 3;
        mHitAngleObject.rigidbody.useGravity = false;

        resetHitAngleObjectPosition();

        mCamDistance = 200.0f;
    }

    public void resetHitAngleObjectPosition()
    {
        mHitAngleObject.transform.position = mCamera.transform.position;
    }
    //TODO mCamDistance should relax when the ball is hit
    public void updateZoom(float aZoom)
    {
        mCamDistance += aZoom * (-100);
        //TODO these bounds should be dynamic somehow
        mCamDistance = Mathf.Clamp(mCamDistance, 20, 2000);
        Vector3 desired = mSight.transform.position + (mCamera.transform.position - mSight.transform.position).normalized * mCamDistance;
        Vector3 direction = desired - mCamera.transform.position;
        if(direction.magnitude > 20.0f)
            mCamera.rigidbody.AddForce((direction) * 0.01f);
    }

    public void update()
    {
        mCamera.transform.LookAt(mSight.transform);

        //make sure 
        Vector3 cam = mCamera.transform.position - mSight.transform.position;
        Vector3 dir = mHitAngleObject.transform.position - mSight.transform.position;
        mHitAngleObject.transform.position = mSight.transform.position + dir * cam.magnitude / dir.magnitude;
    }

    public void setSightObjectPosition(Vector3 pos)
    {
        mSight.transform.position = pos;
    }

    public Vector3 getHitDirection()
    {
        return (mSight.transform.position - mHitAngleObject.transform.position).normalized;
    }

    public void updateRot(Vector3 aChange)
    {
        float y = -aChange.y / 700.0f;
        float x = aChange.x / 700.0f;
        //Vector3 oldPos = mCamera.transform.position;
        mCamera.transform.LookAt(mSight.transform);
        Vector3 targetPos = AnimalMath.rotateAbout(mCamera.transform.position, mCamera.transform.up, mSight.transform.position, y, x );
        Vector3 camForce = (targetPos - mCamera.transform.position)/ 6.0f;
        Vector3 targetForce = (mCamera.rigidbody.position - mHitAngleObject.rigidbody.position) * camForce.magnitude * 1 / 5f;
        mCamera.rigidbody.AddForce(camForce);
        mHitAngleObject.rigidbody.AddForce(camForce + targetForce);
    }
}
