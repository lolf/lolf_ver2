using UnityEngine;
using System.Collections;

//this class handles all mouse related non-sense
public class AnimalMouseManager
{
    Vector3 mStart;
    Vector3 mCurrent;
    bool mCursorDown;
    float mTimeDown;

    public bool isCursorDown
    {
        set { }
        get { return mCursorDown; }
    }
    public void mousePressed(Vector3 aM)
    {
        mCursorDown = true;
        mStart = mCurrent = aM;
        mTimeDown = Time.time;
    }
    public float getTimeDown()
    {
        if (!isCursorDown)
            return 0;
        return Time.time - mTimeDown;
    }
    public float mouseReleased(Vector3 aM)
    {
        mCursorDown = false;
        mCurrent = aM;
        return Time.time - mTimeDown;
    }
    public void mouseMoved(Vector3 aM)
    {
        mCurrent = aM;
    }
    public Vector3 getMouseChange()
    {
        return mCurrent - mStart;
    }
};