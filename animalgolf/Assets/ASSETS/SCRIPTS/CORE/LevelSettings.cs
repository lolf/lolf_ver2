using UnityEngine;
using System.Collections;

public class LevelSettings : MonoBehaviour
{
	//----------
	//modify these in inspector
	//----------
    //pyhsics
    public Vector3 GRAVITY;

    //rendering
    public bool USE_FOG;

    //normalizations
    public float OBJECT_SCALE; //determines when an object is "stopped"
    public float LEVEL_SCALE; //determines how hard to hit
	public float STARTING_CAMERA_DISTANCE = 200.0f;
	public float UPWARD_CAMERA_FORCE = 0.03f;
    
    //audio
	//TODO move delte these from here.. .they are now in animal references
    //TODO public float AUDIO_LEVEL_SCALE determines how to set sound speed
    public AudioClip HIT_SOUND;
    public AudioClip CAN_HIT_SOUND;
    public AudioClip HIT_HARDER_SOUND;
    public AudioClip SINK_SOUND; //when the ball enters the hole.
    public AudioClip SUCCESS_SOUND; //When you golfed well.

    //other stuff
    public string NEXT_SCENE;
	
	
	
	//----------
	//do not modify these in inspector
	//----------
    public GameObject mGoal;
    public GameObject mBall;
    public Camera mGameCamera;
    public GameObject mScripts;
    public Camera mGuiCamera;


	//----------
	//utility functions
	//----------
    static Vector3 defaultUpVector(Vector3 aWorldPosition) { return Vector3.up; }
    static Vector3 defaultGravityVector(Vector3 aWorldPosotion) { return Physics.gravity; }
    public delegate Vector3 getVectorAtVector(Vector3 aWorldPosition);
    public getVectorAtVector getUpVector = new getVectorAtVector(defaultUpVector);
    public getVectorAtVector getGravityVector = new getVectorAtVector(defaultGravityVector);
    public float getScaledDistance(Vector3 aPos1, Vector3 aPos2)
    {
        return (aPos1-aPos2).magnitude/LEVEL_SCALE;
    }

    public float getScaledDistanceFromeGoal()
    {
        return getScaledDistance(mBall.transform.position, mGoal.transform.position);
    }
 
	public AnimalReferences getReferences() { return GetComponentInChildren<AnimalReferences>(); }
	
	
	
	//------------
	//other
	//------------
    void Start()
    {
        Physics.gravity = GRAVITY;
        RenderSettings.fog = USE_FOG;

        mBall = GameObject.FindGameObjectWithTag("BALL");
        mGoal = GameObject.FindGameObjectWithTag("GOAL");
        mGameCamera = GameObject.FindGameObjectWithTag("CAMERA").camera;
        mGuiCamera = GameObject.FindGameObjectWithTag("GUI").camera;
        mScripts = GameObject.FindGameObjectWithTag("SCRIPTS");

    }
}
