using UnityEngine;
using System.Collections.Generic;

public class AnimalMotionTimer
{
    public GameObject mBall;
    public LinkedList<Vector3> mLastPosition = new LinkedList<Vector3>();
    AnimalTimer mTimer = new AnimalTimer(0,1);
    float mScale;
    float mOrigDrag;
    float mOrigAngularDrag;

    public void initialize()
    {
        mScale = GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<LevelSettings>().OBJECT_SCALE;
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mOrigDrag = mBall.rigidbody.drag;
        mOrigAngularDrag = mBall.rigidbody.angularDrag;
        mLastPosition.AddFirst(Vector3.zero * 1);
    }
    public void setTimer(float expire)
    {
        mTimer.setTarget(expire);
    }

    public void resetMotionTimer()
    {
        mBall.rigidbody.drag = mOrigDrag;
        mBall.rigidbody.angularDrag = mOrigAngularDrag;
        mTimer.reset();
    }
    public void update(float delta)
    {
        mTimer.update(delta);
        //TODO should use QuSignal type thing here to take care of the fact that fraamerate might not be uniform
        mLastPosition.AddFirst((mBall.transform.position)*1);
        if (mLastPosition.Count > 150)
            mLastPosition.RemoveLast();

        //TODO this number should change depending on whetehr we are in macro or micro mode and what level we are on
        //TODO make this easy to change ro soemtihng
        //TODO add a time factor in here as well
        
        if (mTimer.getCurrent() > 1.5)
        {
            float dp = computeAbsChange();
            if (dp < 0.5)
            {
                if (dp > 0)
                {
                    mBall.rigidbody.drag = mBall.rigidbody.angularDrag = 5 / dp;
                }
                else mBall.rigidbody.drag = mBall.rigidbody.angularDrag = 100;
            }
        }
    }

    public float computeAbsChange()
    {
        float r = 0;
        Vector3 p = mLastPosition.First.Value;
        foreach (Vector3 e in mLastPosition)
        {
            r += ((p - e).magnitude);
            p = e;
        }
        return r/mLastPosition.Count/mScale;
    }
    public bool isExpired()
    {
        return mTimer.isExpired();
    }
    public bool isStabilized(float thresh = 0.01f)
    {
        return computeAbsChange() < thresh;
        //return (mLastPosition - mBall.transform.position).magnitude < thresh;
    }
    //this one will reduce the stabilized parameter proportional to square root of time
    public bool isTimeBasedStabalized(float mult = 1)
    {
        if (!mTimer.isExpired())
            return false;
        return isStabilized(0.01f + mult * (Mathf.Sqrt(mTimer.getCurrent()) / 50.0f));
    }
}
