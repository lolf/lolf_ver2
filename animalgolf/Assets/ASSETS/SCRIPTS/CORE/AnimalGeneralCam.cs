using UnityEngine;
using System.Collections;

public class AnimalGeneralCam : MonoBehaviour {
    AnimalMouseManager mMouse = new AnimalMouseManager();
    AnimalMouseManager mHitMouse = new AnimalMouseManager();

    Camera mCamera;
    GameObject mBall;
    GameObject mSight;
    LevelSettings mSettings;
	
	Transform birdsEyeView;
	
	public class SaveableTransform
	{
		public Vector3 position;
		public Vector3 eulerAngles;
		public Quaternion rotation;
		
		public SaveableTransform(Transform t)
		{
			this.position = t.position;
			this.rotation = t.rotation;
			this.eulerAngles = t.eulerAngles;
		}
	}
	
	SaveableTransform preZoomOutTransform = null;
	float zoomOutStartTime = float.NegativeInfinity;
	

	System.Collections.Generic.Queue<Vector3> mPreviousHitDirection = new System.Collections.Generic.Queue<Vector3>();

    float mHitAngle = Mathf.PI/4;

    AnimalTimer mTimer = new AnimalTimer(0,3);
    float mCamDistance;

    void Start()
    {
        mSettings = GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<LevelSettings>();
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mSight = new GameObject("genSightObject");
        //TODO mSight should be pulled from the targetting script
        mSight.transform.parent = mBall.transform;
        mSight.transform.localPosition = Vector3.zero;

        mCamera.gameObject.AddComponent<Rigidbody>();
        mCamera.gameObject.AddComponent<SphereCollider>().radius = 10;
        //mCamera.rigidbody.freezeRotation = true;
        mCamera.rigidbody.useGravity = false;
        mCamera.rigidbody.drag = 3;
        mCamera.rigidbody.mass = 0.001f;

        mCamDistance = mSettings.STARTING_CAMERA_DISTANCE;
		
		birdsEyeView = GameObject.Find("BIRDS_EYE_VIEW").transform;
		
    }
	
	public void addExplosiveForce(float amnt)
	{
		mTimer.reset();
		mCamera.rigidbody.AddForce(amnt*mSettings.getUpVector(mBall.transform.position));
	}
	
	float getZoomForceMultiplier()
	{
		return 0.001f + mTimer.getCubed() * 0.01f;
	}
    public void updateZoom(float aZoom)
    {
        mCamDistance += aZoom * (-100);
        //TODO these bounds should be dynamic somehow????
        mCamDistance = Mathf.Clamp(mCamDistance, 20, 2000);
        Vector3 desired = mSight.transform.position + (mCamera.transform.position - mSight.transform.position).normalized * mCamDistance;
        Vector3 direction = desired - mCamera.transform.position;
        if (direction.magnitude > 20.0f)
            mCamera.rigidbody.AddForce((direction) * getZoomForceMultiplier());
    }
	
	public Vector3 getLaggedHitDirection()
	{
		// original
		//Vector3 up = mSettings.getUpVector(mBall.transform.position);
		//Vector3 hitDir =  mPreviousHitDirection.Peek();
		//Vector3 arrowDir = up.normalized * Mathf.Sin(mHitAngle) + hitDir.normalized * Mathf.Cos(mHitAngle);
        //return arrowDir;
		
		Vector3 camPos = this.preZoomOutTransform == null ? mCamera.transform.position : preZoomOutTransform.position;
		
		Vector3 up = mSettings.getUpVector(mBall.transform.position);
        Vector3 hitDir = Vector3.Exclude(up, mBall.transform.position - camPos);
        Vector3 arrowDir = up.normalized * Mathf.Sin(mHitAngle) + hitDir.normalized * Mathf.Cos(mHitAngle);
        return arrowDir;
	}
    public Vector3 getHitDirection()
    {
        Vector3 up = mSettings.getUpVector(mBall.transform.position);
        Vector3 hitDir = Vector3.Exclude(up, mBall.transform.position - mCamera.transform.position);
        Vector3 arrowDir = up.normalized * Mathf.Sin(mHitAngle) + hitDir.normalized * Mathf.Cos(mHitAngle);
        return arrowDir;
    }
    public void setSightPosition(Vector3 aPos)
    {
        mSight.transform.position = aPos;
    }
    void updateRot(Vector3 aChange)
    {
        float y = -aChange.y / 700.0f;
        float x = aChange.x / 700.0f;
        //Vector3 oldPos = mCamera.transform.position;
        mCamera.transform.LookAt(mSight.transform,mSettings.getUpVector(mBall.transform.position));
        Vector3 targetPos = AnimalMath.rotateAbout(mCamera.transform.position, mCamera.transform.up, mSight.transform.position, y, x);
        Vector3 camForce = (targetPos - mCamera.transform.position) / 6.0f;
        mCamera.rigidbody.AddForce(camForce);
        //mHitAngleObject.rigidbody.AddForce(camForce + targetForce);
    }
     
    //this updates rotation of the hit angle object based on mouse input
    //TODO this needs to not allow for 360 degree rotation
    void updateHitRot(Vector3 aChange)
    {
        float y = -aChange.y / 400.0f;
        mHitAngle = Mathf.Clamp(mHitAngle + y, -Mathf.PI / 4, Mathf.PI);
    }

    Vector3 getLookAtPosition()
    {
        Vector3 up = mSettings.getUpVector(mBall.transform.position);
        float heightModifier = Mathf.Clamp(Mathf.Sqrt(mSettings.getScaledDistanceFromeGoal()) * 1.7f, 10, 70);
        return mSight.transform.position + up * heightModifier;
    }
	
	
	void FixedUpdate()
	{
		mPreviousHitDirection.Enqueue((mBall.transform.position - mCamera.transform.position).normalized);
		if(mPreviousHitDirection.Count > 0.2f/Time.fixedDeltaTime)
		{
			mPreviousHitDirection.Dequeue();
		}
	}
	
	void Update()
	{
	
	
		
		//Code for map zoom out, overview /////////////////////////////////
		// --- bullet proof ugly version
		
		if (Input.GetKeyDown(KeyCode.C))
		{
			this.preZoomOutTransform = new SaveableTransform(this.mCamera.transform);
		}
		else if (Input.GetKeyUp(KeyCode.C))
		{
			mCamera.transform.position = preZoomOutTransform.position;
			mCamera.transform.eulerAngles = preZoomOutTransform.eulerAngles;
			this.preZoomOutTransform = null;
		}
		
		if (Input.GetKey(KeyCode.C))
		{
			this.mCamera.transform.position = this.birdsEyeView.position;
			this.mCamera.transform.eulerAngles = Vector3.Lerp(preZoomOutTransform.eulerAngles, this.birdsEyeView.eulerAngles, (Time.time - this.zoomOutStartTime));//this.birdsEyeView.eulerAngles;
			//this.mCamera.transform.rotation = this.birdsEyeView.rotation;//this.birdsEyeView.eulerAngles;
			return;
		} 
		
		// ----------- Pretty ease-in-ease-out version
		/*
		float zoomTime = .5f;
		
	if (Input.GetKeyDown(KeyCode.C) && preZoomOutTransform == null)
		{
			this.preZoomOutTransform = new SaveableTransform(this.mCamera.transform);
			this.zoomOutStartTime = Time.time;
			//preZoomOutTransform.position = mCamera.transform.position;
			//preZoomOutTransform.eulerAngles = mCamera.transform.eulerAngles;
		}
		else if (Input.GetKeyUp(KeyCode.C) && Time.time - this.zoomOutStartTime > zoomTime)
		{
			mCamera.transform.position = preZoomOutTransform.position;
			mCamera.transform.eulerAngles = preZoomOutTransform.eulerAngles;
			this.zoomOutStartTime = Time.time - (zoomTime - Mathf.Min(Time.time - this.zoomOutStartTime, zoomTime));
		}
		
		if (Input.GetKey(KeyCode.C))
		{
			float t = (Time.time - this.zoomOutStartTime)/zoomTime;
			this.mCamera.transform.position = Vector3.Lerp(preZoomOutTransform.position, this.birdsEyeView.position, t);
			//this.mCamera.transform.eulerAngles = Vector3.Lerp(preZoomOutTransform.eulerAngles, this.birdsEyeView.eulerAngles, (Time.time - this.zoomOutStartTime));//this.birdsEyeView.eulerAngles;
			this.mCamera.transform.rotation = Quaternion.Lerp(preZoomOutTransform.rotation, this.birdsEyeView.rotation, t);//this.birdsEyeView.eulerAngles;
			return;
		} 
		else if (Time.time - this.zoomOutStartTime < zoomTime)
		{
			float t = (Time.time - this.zoomOutStartTime)/zoomTime;
			this.mCamera.transform.position = Vector3.Lerp(this.birdsEyeView.position, preZoomOutTransform.position, t);
			//this.mCamera.transform.eulerAngles = Vector3.Lerp(this.birdsEyeView.eulerAngles, preZoomOutTransform.eulerAngles, (Time.time - this.zoomOutStartTime));//this.birdsEyeView.eulerAngles;
			this.mCamera.transform.rotation = Quaternion.Lerp(this.birdsEyeView.rotation, preZoomOutTransform.rotation, t);//this.birdsEyeView.eulerAngles;
			return;
		}
		else
		{
			preZoomOutTransform = null;
		}
		*/
		/////////////////////////////////
		
		
        //TODO separate this into its own class
		mTimer.update(Time.deltaTime);
		
		float rotateInput = Input.GetAxis("Horizontal");
		float hitAngleInput = Input.GetAxis("Vertical");
		
		Vector3 arrowRotationVec = Vector3.zero;
		arrowRotationVec.x = 4*rotateInput;
		arrowRotationVec.y = -hitAngleInput;
		
		Vector3 cameraRotationVec = arrowRotationVec;
		cameraRotationVec.y *= -3;
		
		updateHitRot(arrowRotationVec);
		updateRot(cameraRotationVec);
		
		
        //set the camera
        if (Input.GetMouseButtonDown(0))
		{
            mHitMouse.mousePressed(Input.mousePosition);
		}
			
		if (Input.GetMouseButtonUp(0))
		{
            mHitMouse.mouseReleased(Input.mousePosition);
		}
		else if (Input.GetMouseButton(0))
        {
            Vector3 pos = mHitMouse.getMouseChange();
            mHitMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mHitMouse.getMouseChange();
            Vector3 dp = npos - pos;
            updateHitRot(dp);
			
			dp.y = -dp.y;
			dp.y -= npos.y * 0.01f;
			dp.x += npos.x * 0.01f;
			updateRot(dp);
        }

        if (Input.GetMouseButtonDown(1))
            mMouse.mousePressed(Input.mousePosition);
        if (Input.GetMouseButtonUp(1))
            mMouse.mouseReleased(Input.mousePosition);
        else if (Input.GetMouseButton(1))
        {
            Vector3 pos = mMouse.getMouseChange();
            mMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mMouse.getMouseChange();
            Vector3 dp = npos - pos;
            updateRot(dp);

        }
        float zoom = Input.GetAxis("Mouse ScrollWheel");
        updateZoom(zoom);

        //eyes on the target
        Vector3 up = mSettings.getUpVector(mBall.transform.position);
        mCamera.transform.LookAt(getLookAtPosition(),up);
		
		Vector3 toCamera = (mCamera.transform.position - mBall.transform.position).normalized;
		float angle = Vector3.Angle(up,toCamera);
		if(Mathf.Abs(angle-45) > 5)
		{
			updateRot(new Vector3(0,(angle-45)*-0.05f,0));
		}
	}
}
