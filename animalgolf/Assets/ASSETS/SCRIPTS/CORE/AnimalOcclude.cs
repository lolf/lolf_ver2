using UnityEngine;
using System.Collections;

//this class handles the occlussion camera
//TODO consider switching this to a non mono behaviour type class
public class AnimalOcclude : MonoBehaviour{
    Camera mOccludeCamera;
    Camera mBaseCamera;
    GameObject mArrow;
    GameObject mBaseArrow;
	public bool mEnableOcclusion = false;
	// Use this for initialization
	public void Start () {
		if(!mEnableOcclusion) return;
        mOccludeCamera = GameObject.FindGameObjectWithTag("OCCLUDE").camera;
        //mOccludeCamera = (new GameObject("GenOccludeCamera")).AddComponent<Camera>();
        //mOccludeCamera.transform.position = new Vector3(-999999,999999,999999);
        mBaseCamera = GameObject.FindGameObjectWithTag("CAMERA").camera;
        mBaseArrow = GameObject.FindGameObjectWithTag("ARROW");
        mArrow = (GameObject)GameObject.Instantiate(mBaseArrow);
        mArrow.SetActiveRecursively(mBaseArrow.active);
        mArrow.transform.parent = mOccludeCamera.transform;
        mOccludeCamera.camera.fov = mBaseCamera.fov;
	}
	
	// Update is called once per frame
    public void LateUpdate()
    {
		if(!mEnableOcclusion) return;
        //this is dumb, you should be able to do this with only one line of code
        mOccludeCamera.transform.rotation = mBaseCamera.transform.rotation;
        mArrow.transform.rotation = mBaseArrow.transform.rotation;
        mArrow.transform.parent = null;

        //no form of global scal esupporte???? :(
        Transform op = mBaseArrow.transform.parent;
        mBaseArrow.transform.parent = null;
        mArrow.transform.localScale = mBaseArrow.transform.localScale;
        mArrow.transform.parent = mOccludeCamera.transform;
        mBaseArrow.transform.parent = op;
        //

        mArrow.transform.localPosition = mBaseCamera.transform.InverseTransformPoint(mBaseArrow.transform.position);
        mArrow.SetActiveRecursively(mBaseArrow.active);
    }
}
