using UnityEngine;
using System.Collections;

public class AnimalReferences : MonoBehaviour {
	public GameObject TRAJECTORY_ARROW;
    public GameObject EXPLOSION_ARROW;
    public Material OPAQUE_ARROW_MATERIAL;
    public Material TRANSPARENT_ARROW_MATERIAL;
    public AudioClip[] POWER_SOUNDS;
	
	
	public AudioClip HIT_SOUND;
    public AudioClip CAN_HIT_SOUND;
    public AudioClip HIT_HARDER_SOUND;
    public AudioClip SINK_SOUND; //when the ball enters the hole.
    public AudioClip SUCCESS_SOUND; //When you golfed well.
}
