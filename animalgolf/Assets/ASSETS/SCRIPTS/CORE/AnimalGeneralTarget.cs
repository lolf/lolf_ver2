using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimalGeneralTarget : MonoBehaviour {
	
	//TODO has been moved to AnimalLevelMarkerManager, now use that class...
    int mLastArrowIlluminated = 0;
    int mNumArrows = 10;
    int mNumExplosions = 3;
    GameObject mArrowContainer;
    System.Collections.Generic.List<GameObject> mArrowObjects = new System.Collections.Generic.List<GameObject>();
    System.Collections.Generic.List<GameObject> mExplosionObjects = new System.Collections.Generic.List<GameObject>();
	
	
	
    GameObject mBall;
    Camera mCamera;

    LevelSettings mSettings;
	AnimalReferences mReferences;
    public LevelSettings getSettings() { return mSettings; }
	AnimalGameState mState = new AnimalGameState();
	public AnimalGameState getGameState() { return mState; }
	AnimalDelegator mDelegator = new AnimalDelegator();
	

    AnimalMouseManager mRightMouse = new AnimalMouseManager();
    AnimalMicroTarget mAnimalTarget = new AnimalMicroTarget();
    AnimalMotionTimer mAnimalMotionTimer = new AnimalMotionTimer();
    AnimalGeneralCam mCamPhys;

    GameObject mHitEmit;
    AudioSource mSource;

    bool mSetCursorPositionMode = false;
    RaycastHit mLastHit;
    bool mCanHit = false;
    float mMaxLinearHitForce = 1.9f; //TODO pull this from settings instead?

   

    // Use this for initialization
    void Start()
    {
        mSettings = GetComponent<LevelSettings>();
		mReferences = mSettings.getReferences();
		
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();
        mCamPhys = GetComponent<AnimalGeneralCam>();

        mAnimalTarget.initialize();
        mAnimalMotionTimer.initialize();

        tempStart();
    }

    //contains stuff we should farm out...
    void tempStart()
    {
        mHitEmit = GameObject.FindGameObjectWithTag("EFFECTS").GetComponent<EffectsList>().fx[0];//.GetComponent<ParticleEmitter>();
        mSource = mBall.AddComponent<AudioSource>();
        mSource.volume = 1;
        mSource.clip = mSettings.HIT_SOUND;

        mArrowContainer = new GameObject("genArrowContainer");
        for (int i = 0; i < mNumExplosions; i++)
        {
            GameObject ar = ((GameObject)GameObject.Instantiate(mReferences.EXPLOSION_ARROW));
            mExplosionObjects.Add(ar);
            ar.transform.parent = mArrowContainer.transform;
        }
        for (int i = 0; i < mNumArrows; i++)
        {
            GameObject ar = ((GameObject)GameObject.Instantiate(mReferences.TRAJECTORY_ARROW));
            mArrowObjects.Add(ar);
            ar.transform.parent = mArrowContainer.transform;
        }
        mArrowContainer.SetActiveRecursively(false);
    }
	
	public void teleportBallToGoal()
	{
		//TODO this needs to disable animalmotiontimer friction freezing
        mBall.transform.position = mSettings.mGoal.transform.position + mSettings.getUpVector(mSettings.mGoal.transform.position) * 200;
	}

    // Update is called once per frame
    void Update() {
        //determine if we can hit or not
        bool canHit = mAnimalMotionTimer.isTimeBasedStabalized();
        mAnimalMotionTimer.update(Time.deltaTime);
        if (!mCanHit && canHit)
        {
            //mAnimalTarget.noMouseSetCursorPosition();
            if (!mAnimalTarget.isSet)
            {
                mAnimalTarget.noMouseSetCursorPosition();
            }
            
            mCanHit = true;
            mSource.PlayOneShot(mSettings.CAN_HIT_SOUND);
            mCamPhys.setSightPosition(mBall.transform.position);
            Debug.Log("can hit");
            //set our arrows transparent
            mLastArrowIlluminated = 0;
            for (int i = 0; i < mNumArrows; i++)
                mArrowObjects[i].renderer.material = mReferences.TRANSPARENT_ARROW_MATERIAL;
        }
        if (mCanHit)
        {
            mAnimalTarget.update();

            //TODO consider not spamming arrows
            mArrowContainer.SetActiveRecursively(true);
            foreach (GameObject e in mExplosionObjects)
                e.SetActiveRecursively(false);

            //TODO draw arrows here
            int explosionCounter = 0;
            float stepDistance = 70f;
            float timeStepSize = 0.0025f;
            Vector3 pt = mAnimalTarget.getHitPosition();
            //Vector3 pt = mBall.transform.position;
            //Vector3 dir = mAnimalTarget.getHitDirection();
            Vector3 dir = mCamPhys.getLaggedHitDirection();
            for (int i = 0; i < mNumArrows; i++ )
            {
                dir = dir * stepDistance + mSettings.getGravityVector(pt) *timeStepSize * mSettings.LEVEL_SCALE;
                dir.Normalize();
                Ray ry = new Ray(pt, dir);
                RaycastHit hit;
                float dist = stepDistance;
                int layer = 1 << 0;
                int breakcounter = 0;
                while (dist > 0.01f && Physics.Raycast(ry, out hit, dist, layer) && breakcounter < 3)
                {
                    breakcounter++;
                    dir = Vector3.Reflect((hit.point-pt), hit.normal).normalized;
                    dist -= (hit.point - pt).magnitude;
                    pt = hit.point;
                    ry = new Ray(pt, dir);
                    if (explosionCounter < mNumExplosions)
                    {
                        mExplosionObjects[explosionCounter].transform.position = pt;
                        mExplosionObjects[explosionCounter].SetActiveRecursively(true);
                        explosionCounter++;
                    }

                }
                pt = pt + dir * dist;
                mArrowObjects[i].transform.position = pt;
                mArrowObjects[i].transform.LookAt(pt + dir);
            }
        }
        else
        {
            //TODO consider not spamming these functions
            mArrowContainer.SetActiveRecursively(false);
            mAnimalTarget.hideArrow(true);
        }


        //TODO separate this into its own class
        //set the camera
        /*
        if (Input.GetMouseButtonDown(1))
        {
            mRightMouse.mousePressed(Input.mousePosition);
            Vector3 pos = mRightMouse.getMouseChange();
            mRightMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mRightMouse.getMouseChange();
            Vector3 dp = npos - pos;
        }
        if (Input.GetMouseButtonUp(1))
            mRightMouse.mouseReleased(Input.mousePosition);
        else if (Input.GetMouseButton(1))
        {
            Vector3 pos = mRightMouse.getMouseChange();
            mRightMouse.mouseMoved(Input.mousePosition);
            Vector3 npos = mRightMouse.getMouseChange();
            Vector3 dp = npos - pos;
            mCamPhys.updateRot(dp);

        }
        float zoom = Input.GetAxis("Mouse ScrollWheel");
        mCamPhys.updateZoom(zoom);
        mCamPhys.update();*/
	}

    public void restartLevel()
    {
        Application.LoadLevel(Application.loadedLevelName);
    }

    public void hitBall(float force)
    {
        if (mCanHit && !mAnimalTarget.isTargetting())
        {
            //we can't hit anymore, reset the hit timer
            mCanHit = false;
            
			float hitForce = getHitForce(mAnimalTarget.mArrowStretch);
			
            //TODO determine what hitting mode we are in
            mBall.rigidbody.AddForceAtPosition(mCamPhys.getHitDirection() * hitForce, mAnimalTarget.getHitPosition(), ForceMode.Impulse);
            //mBall.rigidbody.AddForce(mCamPhys.getHitDirection() * getHitForce(mAnimalTarget.mArrowStretch), ForceMode.Impulse);
			
			mAnimalTarget.isSet = false;
            mAnimalTarget.hideArrow(true);
            mAnimalMotionTimer.resetMotionTimer();

			Debug.Log(hitForce);
			mCamPhys.addExplosiveForce(hitForce*mSettings.UPWARD_CAMERA_FORCE);
			
            mHitEmit.transform.position = mAnimalTarget.getHitPosition();
            mHitEmit.SendMessage("Emit", (int)(force / 100.0f));
            mSource.PlayOneShot(mSettings.HIT_SOUND);
			
			if(mDelegator.mBallHit != null)
				mDelegator.mBallHit();
        }
    }

    float getHitForce(float linearHitForce)
    {
		float constantTerm = 8; //TODO make this a per level parameter maybe???
        return Mathf.Pow(linearHitForce * 10 + constantTerm, 1.6666f) * 30f * mSettings.LEVEL_SCALE;
    }
	
    public void setArrows(float force)
    {
        if(mCanHit && !mAnimalTarget.isTargetting())
        {
            mAnimalTarget.mArrowStretch = Mathf.Clamp(force * 100 * 0.01f, 0, mMaxLinearHitForce);
            mAnimalTarget.update();
            int numArrowsToOpaque = (int)(mNumArrows * Mathf.Pow(mAnimalTarget.mArrowStretch / mMaxLinearHitForce,1.6666f));
            if (mLastArrowIlluminated > numArrowsToOpaque)
            {
                mLastArrowIlluminated = numArrowsToOpaque;
            }
            for (int i = 0; i < mNumArrows; i++)
            {
                if (i < numArrowsToOpaque)
                {
                    if (mLastArrowIlluminated <= i)
                    {
                        mLastArrowIlluminated++;
                        //TODO switch this to use resource so you can do it by fielanme
                        if (i < mReferences.POWER_SOUNDS.Length)
                            mSource.PlayOneShot(mReferences.POWER_SOUNDS[i]);
                    }
                    mArrowObjects[i].renderer.material = mReferences.OPAQUE_ARROW_MATERIAL;
                }
                else
                {
                    mArrowObjects[i].renderer.material = mReferences.TRANSPARENT_ARROW_MATERIAL;
                }
            }
        }
    }
}
