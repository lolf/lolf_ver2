using UnityEngine;
using System.Collections;

//this class handles targetting the ball with the arrow
public class AnimalMicroTarget
{
    GameObject mArrow;
    GameObject mArrowStart;
    GameObject mArrowTarget;
    public GameObject mBall;
    protected Camera mCamera;
    public bool isSet = false;
    public float mRange = 6;
    bool mSetCursorPositionMode;
    
    Vector3 mArrowOrigScale;
    public float mArrowStretch;

    AnimalMouseManager mLeftMouse = new AnimalMouseManager();
    AnimalMouseManager mRightMouse = new AnimalMouseManager();

    public void initialize()
    {
        isSet = false;
        mArrow = GameObject.FindGameObjectWithTag("ARROW");
        mBall = GameObject.FindGameObjectWithTag("BALL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA").GetComponent<Camera>();
        mArrowTarget = new GameObject("GenArrowTarget");
        mArrowStart = new GameObject("GenArrowStart");
        mArrow.transform.parent = mBall.transform;
        mArrowTarget.transform.parent = mBall.transform;
        mArrowStart.transform.parent = mBall.transform;
        mArrowOrigScale = mArrow.transform.localScale;
        mArrowStretch = 1.0f;        
    }
    public void rotate(Vector3 aChange)
    {
        float y = Mathf.Clamp(aChange.y / 500.0f, -mRange, mRange);
        float x = Mathf.Clamp(-aChange.x / 500.0f, -mRange, mRange);
        mArrow.transform.position = AnimalMath.rotateAbout(mArrowStart.transform.position, mArrowStart.transform.up, mArrowTarget.transform.position, y, x);
        mArrow.transform.LookAt(mArrowTarget.transform.position);
    }
    public void setTargetPosition(Vector3 aPos, Vector3 aNorm)
    {
        isSet = true;
        mArrow.transform.position = aPos + aNorm * 0.1f;
        mArrowStart.transform.position = mArrow.transform.position;
        mArrowTarget.transform.position = aPos;
        mArrow.transform.LookAt(mArrowTarget.transform);
        mArrowStart.transform.LookAt(mArrowTarget.transform);
    }

    bool autoCast(int layer, out RaycastHit hit)
    {
        //Ray toCast = new Ray(mCamera.transform.position, mBall.transform.position);
        Ray toCast = new Ray(mCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), mBall.transform.position);
        return Physics.Raycast(toCast, out hit, Mathf.Infinity, layer);
    }

    bool mouseCast(int layer, out RaycastHit hit)
    {
        Vector3 mouse = Input.mousePosition;
        mouse.x /= (float)Screen.width;
        mouse.y /= (float)Screen.height;
        Ray toCast = mCamera.ViewportPointToRay(mouse);
        return Physics.Raycast(toCast, out hit, Mathf.Infinity, layer);
    }

    public bool noMouseSetCursorPosition()
    {
        RaycastHit hit;
        if (autoCast(1 << 8, out hit))
        {
            setTargetPosition(hit.point, hit.normal);
            return true;
        }
        return false;
    }

    bool setCursorPosition()
    {
        RaycastHit hit;
        if (mouseCast(1 << 8, out hit))
        {
            setTargetPosition(hit.point, hit.normal);
            return true;
        }
        return false;
    }
    public Vector3 getHitDirection()
    {
        return mArrow.transform.forward;
    }
    public Vector3 getHitPosition()
    {
		if(!isSet)
			return mBall.transform.position;
		else
        	return mArrow.transform.position;
    }

    public bool isTargetting()
    {
        return mSetCursorPositionMode;
    }

    public void update()
    {
        //set the arrow
        if (Input.GetMouseButtonDown(0) && !mRightMouse.isCursorDown)
        {
            mRightMouse.mousePressed(Input.mousePosition);
            if (setCursorPosition())
            {
                mSetCursorPositionMode = true;
                mArrow.transform.localScale = mArrowOrigScale;
                hideArrow(false);
            }
        }
        else
        {
            mSetCursorPositionMode = false;
        }
        if (Input.GetMouseButtonUp(0))
            mRightMouse.mouseReleased(Input.mousePosition);
        Vector3 scaled = mArrowOrigScale;
        scaled.z += mArrowStretch;
        //mArrow.transform.localScale = scaled;
    }
    public void hideArrow(bool doHide)
    {
        mArrow.SetActiveRecursively(!doHide);
    }
}
