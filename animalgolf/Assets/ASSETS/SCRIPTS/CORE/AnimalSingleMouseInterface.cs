using UnityEngine;
using System.Collections;

public class AnimalSingleMouseInterface : MonoBehaviour {
	AnimalMouseManager mMouse = new AnimalMouseManager();
	
	LevelSettings mSettings;
	AnimalGeneralTarget mGeneral;
	
	// Use this for initialization
	void Start () {
		mSettings = GetComponent<LevelSettings>();
		mGeneral = GetComponent<AnimalGeneralTarget>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        if (Input.GetKeyDown(KeyCode.Z))
            Application.LoadLevel(mSettings.NEXT_SCENE);
        if (Input.GetKeyDown(KeyCode.F2))
        	mGeneral.teleportBallToGoal();
		
		
		
	}
}
