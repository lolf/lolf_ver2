using UnityEngine;
using System.Collections;

public class AnimalTrajectory {
	AnimalGeneralTarget mScripts;
	
	GameObject mTrajectoryArrow;
    GameObject mExplosionArrow;
    Material mArrowOpaqueMaterial;
    Material mArrowTransparentMaterial;
    AudioClip[] mPowerSounds;
	
    int mNumArrows = 8;
    int mNumExplosions = 3;
	
    GameObject mArrowContainer;
    System.Collections.Generic.List<GameObject> mArrowObjects = new System.Collections.Generic.List<GameObject>();
    System.Collections.Generic.List<GameObject> mExplosionObjects = new System.Collections.Generic.List<GameObject>();
	
	int mLastArrowIlluminated = 0;
	
	public AnimalTrajectory()
	{
		mScripts = GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<AnimalGeneralTarget>();
		LevelSettings s = mScripts.getSettings();
		mTrajectoryArrow = s.getReferences().TRAJECTORY_ARROW;
		mExplosionArrow = s.getReferences().EXPLOSION_ARROW;
		mArrowOpaqueMaterial = s.getReferences().OPAQUE_ARROW_MATERIAL;
		mArrowTransparentMaterial = s.getReferences().TRANSPARENT_ARROW_MATERIAL;
		mPowerSounds = s.getReferences().POWER_SOUNDS;
	}
}