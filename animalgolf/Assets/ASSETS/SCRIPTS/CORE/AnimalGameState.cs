using UnityEngine;
using System.Collections;

public class AnimalGameState 
{
	public enum GameState
	{
		GS_PREVIEW,
		GS_USER,
		GS_SHOOTING,
		GS_WIN
	}
	GameState mState;
	float mStateTime;
	public AnimalGameState()
	{
		mState = GameState.GS_PREVIEW;
		mStateTime = 0; 
	}
	public GameState getState(){return mState;}
	public void setState(GameState aState)
	{
		mState = aState;
		mStateTime = Time.time;
	}
	public float timeOnState(){return Time.time-mStateTime;}
}
