using UnityEngine;
using System.Collections;

public class AnimalBackgroundMusic : MonoBehaviour {

    public bool mFade;
    public float mFadeThresh;
    public AudioClip mClip;
    public float mAudioVolume;

    public AnimalTimer mBallInFadeTimer = new AnimalTimer(0, 0);
    
    AudioSource mSource;
	// Use this for initialization
	void Start () {
        mSource = gameObject.AddComponent<AudioSource>();
        mSource.loop = true;
        mSource.clip = mClip;
        mSource.Play();
        mSource.volume = mAudioVolume;
        if (mFade)
            mSource.volume = 0;
        
	}

    public void fadeOutPermanently()
    {
        mBallInFadeTimer = new AnimalTimer(0, 1);
    }
	// Update is called once per frame
	void FixedUpdate () {
        float v = 1;
        if (mFade)
        {
            if(mSource.time < mFadeThresh)
                v = mSource.time/mFadeThresh;
            else if (mClip.length - mSource.time < mFadeThresh)
                v = (mClip.length - mSource.time)/mFadeThresh;
            v *= mAudioVolume;
        }
        if(mBallInFadeTimer.isInitialized())
        {
            mBallInFadeTimer.update(Time.fixedDeltaTime);
            v *= (1 - mBallInFadeTimer.getLinear());
        }
        //TODO do second fade here, maybe in general you should consider making a music fade manager such that several scripts can fade one sound...

        mSource.volume = v;
	}
}
