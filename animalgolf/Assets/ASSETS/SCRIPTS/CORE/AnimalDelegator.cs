using UnityEngine;
using System.Collections;

public class AnimalDelegator 
{
	//declare your delegate types here
	public delegate void BallHit();
	public delegate void BallStabalized();
	public delegate void BallIn();
	
	
	//delegates here, public so anyone can call events
	public BallHit mBallHit;
	public BallStabalized mBallStabalized;
	public BallIn mBall;
	
	
	
}
