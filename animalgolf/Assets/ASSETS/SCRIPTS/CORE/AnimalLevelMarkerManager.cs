using UnityEngine;
using System.Collections;

public class AnimalLevelMarkerManager : MonoBehaviour {
	System.Collections.Generic.List<GameObject> mArrowObjects = new System.Collections.Generic.List<GameObject>();
    System.Collections.Generic.List<GameObject> mExplosionObjects = new System.Collections.Generic.List<GameObject>();
	GameObject mArrowContainer;
	public int mNumArrows = 10;
	
	
	LevelSettings mSettings;
	AnimalReferences mReferences;
	
	// Use this for initialization
	void Start () 
	{
		mSettings = GetComponent<LevelSettings>();
		mReferences = mSettings.getReferences();
	    mArrowContainer = new GameObject("genArrowContainer");
        for (int i = 0; i < mNumArrows; i++)
        {
            GameObject ar = ((GameObject)GameObject.Instantiate(mReferences.TRAJECTORY_ARROW));
            mArrowObjects.Add(ar);
            ar.transform.parent = mArrowContainer.transform;
        }
        mArrowContainer.SetActiveRecursively(false);
	}

    public Vector3 computeNearestPoint(Vector3 aPos)
    {
        //TODO
        return Vector3.zero;
    }

}
