using UnityEngine;
using System.Collections;

public class VelocityParticleEmitter : MonoBehaviour {
    public GameObject mEmit;
    public float mScaleQuantity;
    public float mMaxQuantity;
	// Update is called once per frame
	void Update () {
        float thresh = 60;
        float mag = rigidbody.velocity.magnitude;
        if(mag > thresh)
        {
            int quantity = (int)Mathf.Clamp((Mathf.Abs(mag - thresh) / 10.0f), 0, mMaxQuantity);
            mEmit.transform.position = transform.position;
	        mEmit.SendMessage("Emit", quantity * mScaleQuantity);
        }
	}
}
