using UnityEngine;
using System.Collections;


//TODO instead of using mTimer to control maximum number of emissions, use a QuAlgebraicSignal to figure out how many particles were dropped in the last __ ms and make sure that quantity is capped
public class BounceParticleEmitter : MonoBehaviour {
    public GameObject mEmit;
    public bool useDefault;
    public float mQuantity;
    public float mMinForce = 100;
    public int mMinEmit = 0;
    public float mReleaseTimer = 0.3f;
    AnimalTimer mTimer;
	// Use this for initialization
	void Start () {
        if (useDefault)
        {
            mEmit = GameObject.FindGameObjectWithTag("EFFECTS").GetComponent<EffectsList>().fx[1];
            mQuantity = 10;
        }
        mTimer = new AnimalTimer(0, mReleaseTimer);
	}
	
	// Update is called once per frame
	void Update () {
        mTimer.update(Time.deltaTime);
	}
    void OnCollisionEnter(Collision collision)
    {
        if (mTimer.isExpired())
        {
            mTimer.reset();
            if (collision.impactForceSum.magnitude > mMinForce)
            {
                int numToEmit = (int)(Mathf.Clamp(collision.impactForceSum.magnitude / 200.0f * mQuantity, 0, 100.0f)) + mMinEmit;
                //int numToEmit = (int) mQuantity/collision.contacts.Length;
                int extra = numToEmit % collision.contacts.Length;
                foreach (ContactPoint e in collision.contacts)
                {
                    mEmit.transform.position = e.point;
                    mEmit.SendMessage("Emit", numToEmit/collision.contacts.Length);
                    if (extra > 0)
                    {
                        mEmit.SendMessage("Emit", 1);
                        extra--;
                    }
                }

            }
        }
    }
}
