using UnityEngine;
using System.Collections;

//this script tells you what direction up is in outer space, c.f. space force
public class SpaceOrienter : MonoBehaviour {

    public GameObject[] mBodies;
    public GameObject mBall;

	// Use this for initialization
	void Start () {
        mBall = GameObject.FindGameObjectWithTag("BALL");
        GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<LevelSettings>().getUpVector = new LevelSettings.getVectorAtVector(getUpVector);
        GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<LevelSettings>().getGravityVector = new LevelSettings.getVectorAtVector(getGravity);
	}
	
	// Update is called once per frame
	public Vector3 getUpVector (Vector3 aPos) 
    {
        Vector3 up = Vector3.zero;
        foreach (GameObject e in mBodies)
        {
            Vector3 dir = e.transform.position - aPos;
            Vector3 force = (dir.normalized * 10000000 * e.rigidbody.mass);
            force = force / (dir.magnitude * dir.magnitude);
            up -= force;
        }
        return up.normalized;
	}

    public Vector3 getGravity(Vector3 aPos)
    {
        Vector3 up = Vector3.zero;
        foreach (GameObject e in mBodies)
        {
            Vector3 dir = e.transform.position - aPos;
            Vector3 force = (dir.normalized * 10000000 * e.rigidbody.mass * mBall.rigidbody.mass);
            force = force / (dir.magnitude * dir.magnitude);
            up += force;
        }
        return up;
    }
}
