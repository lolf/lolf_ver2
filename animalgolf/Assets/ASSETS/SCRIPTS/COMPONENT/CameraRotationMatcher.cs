using UnityEngine;
using System.Collections;

public class CameraRotationMatcher : MonoBehaviour {
    public GameObject mToMatch;
	// Update is called once per frame
	void Update () {
        //transform.rotation = mToMatch.transform.rotation;
        transform.rotation = Quaternion.Slerp(transform.rotation, mToMatch.transform.rotation, 0.1f);
	}
}
