using UnityEngine;
using System.Collections;

public class CheaterForce : MonoBehaviour {

    public GameObject mAttractBody;
    public float mAttractMinDistance;
    public float mAttractDegree;
    public float mAttractConstant;

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 dir = mAttractBody.transform.position - transform.position;
        if (dir.magnitude < mAttractMinDistance)
        {
            Vector3 force = dir.normalized * mAttractConstant;
            force = force / Mathf.Pow(dir.magnitude, mAttractDegree);
            rigidbody.AddForce(force);
        }
    }
}
