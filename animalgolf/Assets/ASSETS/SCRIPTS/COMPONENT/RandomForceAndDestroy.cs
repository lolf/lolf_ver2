using UnityEngine;
using System.Collections;

public class RandomForceAndDestroy : MonoBehaviour {
    Vector3 mBaseScale;
    AnimalTimer mTimer = new AnimalTimer(0,10);
	// Use this for initialization
	public void Start () {
        mBaseScale = transform.localScale;
        if(rigidbody.constantForce)
            rigidbody.constantForce.force = Random.onUnitSphere * Random.Range(20f, 40f);
	}
	
	// Update is called once per frame
	public void Update() {
        mTimer.update(Time.deltaTime);
        transform.localScale = mBaseScale * (1-mTimer.getSquare());
        if (mTimer.isExpired())
            DestroyImmediate(gameObject);
	}
}
