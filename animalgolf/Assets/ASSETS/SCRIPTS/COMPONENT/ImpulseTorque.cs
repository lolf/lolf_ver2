using UnityEngine;
using System.Collections;

public class ImpulseTorque : MonoBehaviour {
    public Vector3 mRotation;
    public float randomStrengthMin;
    public float randomStrengthMax;
    public bool useRandomDirection;
    
	// Use this for initialization
	void Start () {
        if (useRandomDirection)
            mRotation = Random.onUnitSphere * Random.Range(randomStrengthMin, randomStrengthMax);
        rigidbody.AddRelativeTorque(mRotation,ForceMode.Impulse);
	}
}
