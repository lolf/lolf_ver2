using UnityEngine;
using System.Collections;

public class SpaceForce : MonoBehaviour {
    public GameObject[] mBodies;

	// Update is called once per frame
	void FixedUpdate () 
    {
        foreach (GameObject e in mBodies)
        {
            Vector3 dir = e.transform.position - transform.position;
            Vector3 force = (dir.normalized * 10000000 * e.rigidbody.mass * transform.rigidbody.mass);
            force = force / (dir.magnitude * dir.magnitude);
            rigidbody.AddForce(force);
        }
	}
}
