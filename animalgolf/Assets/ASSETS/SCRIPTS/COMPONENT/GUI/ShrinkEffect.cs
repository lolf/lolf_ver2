using UnityEngine;
using System.Collections;

public class ShrinkEffect : MonoBehaviour {
    Vector3 mScale;
	// Use this for initialization
	void Start () {
        mScale = transform.localScale;
	}
    void shrink(float amnt)
    {
        transform.localScale = mScale * amnt;
    }
}
