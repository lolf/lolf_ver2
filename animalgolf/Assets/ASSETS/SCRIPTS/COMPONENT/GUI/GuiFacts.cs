using UnityEngine;
using System.Collections;

public class GuiFacts : MonoBehaviour {
	public string mFact;
	public TextMesh mTextMesh = null;
	
	// Use this for initialization
	void Start () {
		if(!mTextMesh)
		{
			mTextMesh = new TextMesh();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
