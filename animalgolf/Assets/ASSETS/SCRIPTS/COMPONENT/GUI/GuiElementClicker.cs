using UnityEngine;
using System.Collections;

public class GuiElementClicker : MonoBehaviour {
    Camera mCamera;
    AnimalGeneralTarget mScripts;
    int mClickButton = 0;
    public GameObject mHitButton;
    public GameObject mResetButton;
    AnimalMouseManager mHitMouse = new AnimalMouseManager();
    AnimalMouseManager mResetMouse = new AnimalMouseManager();
	// Use this for initialization
	void Start () {
        mScripts = GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<AnimalGeneralTarget>();
        mCamera = camera; //GameObject.FindObject...
	}
    Collider cast()
    {
        RaycastHit hit;
        Vector3 mouse = Input.mousePosition;
        mouse.x /= (float)Screen.width;
        mouse.y /= (float)Screen.height;
        Ray toCast = mCamera.ViewportPointToRay(mouse);
        int layer = 1 << 9; //9 is gui layer
        if (Physics.Raycast(toCast, out hit, Mathf.Infinity, layer))
            return hit.collider;
        return null;
        
    }
	// Update is called once per frame
	void Update () {
		
		
		///// key controls ///////////////////////////////////////////////////
		bool keyboardHitDown = Input.GetButtonDown("hit");
		bool keyboardHitUp = Input.GetButtonUp("hit");
		bool keyDown = keyboardHitDown;
		
		bool resetKeyDown = Input.GetButtonDown("reset");
		
		if (resetKeyDown)
		{
			mScripts.restartLevel();
		}
		
		if (keyboardHitDown)
		{
			mHitMouse.mousePressed(new Vector2(0,0));
		}
		else if (keyboardHitUp)
		{
			float timeHeld = mHitMouse.mouseReleased(new Vector2(0,0));
			mScripts.hitBall(timeHeld);
		}
		
		if (mHitMouse.isCursorDown)
        {
            mScripts.setArrows(mHitMouse.getTimeDown());
        }
		
		/////////////////////////////////////////////////////////////////////
		
		/*
		
        Collider col = cast();
        if (Input.GetMouseButtonDown(mClickButton))
        {
            if (col == mHitButton.collider)
            {
                mHitMouse.mousePressed(Input.mousePosition);
                mHitButton.SendMessage("shrink", 0.9f);
            }
            else if (col == mResetButton.collider)
            {
                mResetMouse.mousePressed(Input.mousePosition);
                mResetButton.SendMessage("shrink", 0.9f);
            }
        }
        
		if (Input.GetMouseButtonUp(mClickButton))
        {
            if (mHitMouse.isCursorDown && col == mHitButton.collider)
			{
                mScripts.hitBall(mHitMouse.mouseReleased(Input.mousePosition));
			}
			
			if (mResetMouse.isCursorDown && col == mResetButton.collider)
            {
                mScripts.restartLevel();
                mResetMouse.mouseReleased(Input.mousePosition);
            }
            mHitButton.SendMessage("shrink", 1);
            mResetButton.SendMessage("shrink", 1);
        }
		
		
        if (mHitMouse.isCursorDown)
        {
			
		        if (col != mHitButton.collider && (!keyboardHitDown || keyboardHitUp))
	            {
	                mHitMouse.mouseReleased(Input.mousePosition);
	                mHitButton.SendMessage("shrink", 1);
	            }
	            if (col != mResetButton.collider)
	            {
	                mResetMouse.mouseReleased(Input.mousePosition);
	                mResetButton.SendMessage("shrink", 1);
	            }
            mScripts.setArrows(mHitMouse.getTimeDown());
        }
        else
        {
            if (col == mHitButton.collider)
                mHitButton.SendMessage("shrink", 0.95f);
            else
                mHitButton.SendMessage("shrink", 1);
            
			if (col == mResetButton.collider)
			{
				mResetButton.SendMessage("shrink", 0.95f);
			}
			else
			{
                mResetButton.SendMessage("shrink", 1);
			}
        }
		*/
	}
}
