using UnityEngine;
using System.Collections;

public class GuiFactAnimation : MonoBehaviour {
	Vector3 mBasePosition;
	bool lateInitialized = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!lateInitialized)
		{
			mBasePosition = transform.position;
			lateInitialized = true;
		}
		transform.position = mBasePosition + (new Vector3(Mathf.Sin(Time.time*3),Mathf.Sin(Time.time*2),0))*5;
		transform.localEulerAngles = new Vector3(transform.localEulerAngles.x,transform.localEulerAngles.y,0 + Mathf.Sin(Time.time*4)*14);
	}
}
