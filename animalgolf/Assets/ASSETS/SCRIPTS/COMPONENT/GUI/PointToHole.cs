using UnityEngine;
using System.Collections;

public class PointToHole : MonoBehaviour {
    GameObject mGuiCamera;
    GameObject mCamera;
    GameObject mGoal;
    GameObject mBall;
	// Use this for initialization

    //TODO consider making this something you can drop on SCRIPTOBJECT instead of directly on the arrow
	void Start () {
        mGuiCamera = GameObject.FindGameObjectWithTag("GUI");
        mGoal = GameObject.FindGameObjectWithTag("GOAL");
        mCamera = GameObject.FindGameObjectWithTag("CAMERA");
        mBall = GameObject.FindGameObjectWithTag("BALL");

        //TODO reposition the arrow so it works with any aspect ratio
	}
	
	// Update is called once per frame
	void LateUpdate () {

        /*Vector3 arrowPos = mGuiCamera.camera.WorldToScreenPoint(transform.position);
        arrowPos.z = mCamera.camera.nearClipPlane;
        Vector3 pos = mCamera.camera.ScreenToWorldPoint(arrowPos);*/

        Vector3 dir  = mGoal.transform.position-mBall.transform.position;
        transform.LookAt(transform.position - dir);
        transform.rotation = Quaternion.Inverse(mCamera.transform.rotation) * transform.rotation;
	}
}
