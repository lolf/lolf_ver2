using UnityEngine;
using System.Collections;

public class GuiElementPositioner : MonoBehaviour {
    public GameObject mButtons;
    public GameObject mGoalDirection;
	public GameObject mAnimalFacts;
    Camera mCamera;
	// Use this for initialization
	void Start () {
        mCamera = GameObject.FindGameObjectWithTag("GUI").camera;
        mButtons.transform.position = mCamera.ScreenToWorldPoint(new Vector3(50, Screen.height -  50, mCamera.nearClipPlane + 100));
        mGoalDirection.transform.position = mCamera.ScreenToWorldPoint(new Vector3(Screen.width * (1 - 0.05f), Screen.height - Screen.width*(0.05f), mCamera.nearClipPlane + 100));
		if(mAnimalFacts != null)
			mAnimalFacts.transform.position = mCamera.ScreenToWorldPoint(new Vector3(Screen.width*0.5f,Screen.height*0.5f,mCamera.nearClipPlane + 100));
	}
}
