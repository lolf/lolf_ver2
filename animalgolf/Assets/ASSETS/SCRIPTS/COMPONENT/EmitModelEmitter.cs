using UnityEngine;
using System.Collections;

public class EmitModelEmitter : MonoBehaviour {
    public GameObject mModel;
    bool mGoodToGo;
    void Start()
    {
        mGoodToGo = false;
        if (mModel.rigidbody != null
            && mModel.constantForce != null)
        {
            mGoodToGo = true;
        }
    }
    void Emit(int q)
    {
        if (mGoodToGo)
        {
            for (int i = 0; i < q; i++)
            {
                ((GameObject)GameObject.Instantiate(mModel, transform.position + Random.onUnitSphere * 20, Random.rotation)).rigidbody.AddForce(Random.onUnitSphere * 30,ForceMode.Impulse);
            }
        }
    }
}
