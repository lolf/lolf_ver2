using UnityEngine;
using System.Collections;

public class ImpulseOrbit : MonoBehaviour {
    public Rigidbody mToOrbit;
    public Vector3 mPerp;
	// Use this for initialization
	void Start () {
        Vector3 dir = mToOrbit.transform.position-transform.position;
        rigidbody.velocity = Vector3.Cross(mPerp, dir).normalized * Mathf.Sqrt( mToOrbit.mass / dir.magnitude) * 3500;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
