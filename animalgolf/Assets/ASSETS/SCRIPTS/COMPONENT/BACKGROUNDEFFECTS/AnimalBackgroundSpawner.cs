using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//this class randomly spawns stuff in the background with a number of preset patterns
public class AnimalBackgroundSpawner : MonoBehaviour {

    //these two arehandled locally
    public GameObject [] mPrefabsToSpawn;
    public float mSpawnRate; //rate of effect

    //these are passed to mPrefabsToSpawn
    public float mEffectDistanceMin; //usually controls distance from camera
    public float mEffectDistanceMax;
    public float mSpeedMin; //speed of each effect
    public float mSpeedMax;
    public float[] mAdditionalArgs; //these are passed directly to mPrefabsToSpawn

    //other local stuff
    Camera mCamera;

	void Start () {
        mCamera = camera; //alternatively, GameObject.FindObjectWithTag("BACKGROUND").GetComponent...
	}
	
	// Update is called once per frame
    // TODO this should keep some sort of timer to determine when to spawn, but conveniently, this is fixed in fixed update
	void FixedUpdate () {
        if (Random.value < mSpawnRate)
        {
            //do something like SendMessage("Emit", quantity * mScaleQuantity);
        }
	}
}
