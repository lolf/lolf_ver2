using UnityEngine;
using System.Collections;

public class EmitParticleEmitter : MonoBehaviour {
    ParticleEmitter mEmit;
	// Use this for initialization
	public void Start () {
        mEmit = GetComponent<ParticleEmitter>();
	}
    public void Emit(int q)
    {
        if (q == -1)
            mEmit.Emit();
        mEmit.Emit(q);
    }
}
