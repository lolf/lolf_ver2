using UnityEngine;
using System.Collections;

public class OutOfBoundsResetter : MonoBehaviour {
	
	AnimalGeneralTarget mScripts;
	GameObject mBall;
	
	// Use this for initialization
	void Start () {
		mScripts = GameObject.FindGameObjectWithTag("SCRIPTS").GetComponent<AnimalGeneralTarget>();
		mBall = GameObject.FindGameObjectWithTag("BALL");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (this.mBall.transform.position.y < this.transform.position.y)
		{
			mScripts.restartLevel();
		}
	}
}
