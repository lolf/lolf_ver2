using UnityEngine;
using System.Collections;

public class Posterize : MonoBehaviour {
	
	public Shader shaderMat;
	
	
	void Start(){
		//tex = new RenderTexture(Screen.width, Screen.height, 1024);
		//this.camera.targetTexture = tex;
		
		shaderMat = Shader.Find("Hidden/Posterize");
	}
	public float val = 1;
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
		Material mat = new Material(shaderMat);
		mat.SetFloat("_steps", Mathf.Sin(Time.time/val)*100);

		Graphics.Blit(source,destination,mat);
	}
}
