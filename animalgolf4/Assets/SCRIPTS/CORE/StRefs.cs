using UnityEngine;
using System.Collections.Generic;

public class StRefs : MonoBehaviour {
	List<GameObject> mGoals;
	List<GameObject> mBalls;
	public GameObject ActiveBall
	{
		get; private set;
	}
	public float BallScale
	{
		get{
			return ActiveBall.renderer.bounds.extents.max();
		}
	}
	public GameObject ActiveGoal
	{
		get; private set;
	}
	void Start () {
		
		mBalls = new List<GameObject>();
		
		//find all goals
		GameObject findball = GameObject.Find("ST_BALL");
		if(findball != null)
			mBalls.Add(findball);
		for(int i = 0; i < 30; i++)
		{
			GameObject findballs = GameObject.Find("ST_BALL_"+i);
			if(findballs != null)
				mBalls.Add(findballs);
			else break;
		}
		if(mBalls.Count > 0)
			ActiveBall = mBalls[0];
		else throw new UnityException("no balls found");
		
		mGoals = new List<GameObject>();
		//find all goals
		GameObject findgoal = GameObject.Find("ST_GOAL");
		if(findgoal != null)
			mGoals.Add(findgoal);
		for(int i = 0; i < 30; i++)
		{
			GameObject findgoals = GameObject.Find("ST_GOAL_"+i);
			if(findgoals != null)
				mGoals.Add(findgoals);
			else break;
		}
		
		if(mGoals.Count > 0)
			ActiveGoal = mGoals[0];
		else throw new UnityException("no goals found");
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
