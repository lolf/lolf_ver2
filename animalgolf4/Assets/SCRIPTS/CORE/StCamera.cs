using UnityEngine;
using System.Collections;

public class StCamera : MonoBehaviour {

	GameObject mBall;
	GameObject mPoV;
	GameObject mOverhead;
	GameObject mScenic;
	GameObject mBegin;
	GameObject mEnd;
	Camera mCam;
	
	StRefs mRefs;
	void Start () {
		
		mBall = new GameObject("genBallCameraPlace");
		mPoV = new GameObject("genPoVCameraPlace");
		mOverhead = new GameObject("genOverheadCameraPlace");
		mScenic = new GameObject("genScenicCameraPlace");
		mBegin = new GameObject("genBeginCameraPlace");
		mEnd = new GameObject("genEndCameraPlace");
		
		mCam = new GameObject("genCamera").AddComponent<Camera>();
		
		mRefs = gameObject.GetComponent<StRefs>();
	}
	
	
	void Update () {
		update_ball_cam_position();
		
		mCam.transform.position = mBall.transform.position;
		mCam.transform.rotation = mBall.transform.rotation;
		
		
	}
	
	
	public void update_ball_cam_position()
	{
		
	}
	
	void reset_ball_cam_position()
	{
		Vector3 upVector = Vector3.up; //TODO
		Vector3 toGoal = Vector3.Exclude(upVector,(mRefs.ActiveGoal.transform.position - mRefs.ActiveBall.transform.position));
		float dFromBall = mRefs.BallScale*1.7f;
		float hFromBall = mRefs.BallScale*1.5f;
		Vector3 fPos = mRefs.ActiveBall.transform.position + upVector * hFromBall + toGoal * (-dFromBall);
		mBall.transform.position = fPos;
		mBall.transform.LookAt(mBall.transform.position + toGoal,upVector);
	}
}
