using UnityEngine;
using System.Collections;

public class StCore : MonoBehaviour {
	
	
	void Awake()
	{
		//Instantiate standard stuff here...
		
		gameObject.AddComponent<StCamera>();	
		gameObject.AddComponent<StBall>();
		gameObject.AddComponent<StGoalDetector>();
		gameObject.AddComponent<StShooter>();
		gameObject.AddComponent<StUI>();
		gameObject.AddComponent<StGame>();
		
	}
	
	void Start () {
	
	}
	
	
	void Update () {
	
	}
}
