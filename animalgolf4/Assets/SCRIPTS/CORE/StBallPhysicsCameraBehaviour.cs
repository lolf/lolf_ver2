using UnityEngine;
using System.Collections;

public class StBallPhysicsCameraBehaviour : MonoBehaviour {
	
	GameObject mBallCamera;
	// Use this for initialization
	void Start () {
		mBallCamera = new GameObject("genBallCamera");
		mBallCamera.AddComponent<SphereCollider>().radius = 0.1f;
		mBallCamera.AddComponent<Rigidbody>();
		mBallCamera.rigidbody.drag = 3;
        mBallCamera.rigidbody.mass = 0.001f;
		mBallCamera.rigidbody.useGravity = false;
		//TODO set position
	}
	
	public void updateZoom(float aZoom)
    {
        mCamDistance += aZoom * (-100);
        //TODO these bounds should be dynamic somehow????
        mCamDistance = Mathf.Clamp(mCamDistance, 0.1f, 20);
        Vector3 desired = mBall.transform.position + (mBallCamera.transform.position - mBall.transform.position).normalized * mCamDistance;
        Vector3 direction = desired - mBallCamera.transform.position;
        if (direction.magnitude > 0.1f)
            mBallCamera.rigidbody.AddForce((direction) * 0.01f);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
