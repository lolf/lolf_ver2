using UnityEngine;
using System.Collections;

public static class ExtVector3 {
	public static float max(this Vector3 v)
	{
		return Mathf.Max(new float[]{v.x,v.y,v.z});
	}
}
